// swift-tools-version:5.0
// The swift-tools-version declares the minimum version of Swift required to build this package.
import PackageDescription

let package = Package(
    name: "code-planner-api",
    products: [
        .executable(name: "Run", targets: ["Run"]),
        .library(name: "App", targets: ["App"]),
    ],
    dependencies: [ 
        // 💧 A server-side Swift web framework.
        .package(url: "https://github.com/vapor/vapor.git", from: "3.3.1"),
        // 🐘 Swift ORM (queries, models, relations, etc) built on PostgreSQL.
        .package(url: "https://github.com/vapor/fluent-postgresql.git", from: "1.0.0"),
        // 👤 Authentication and Authorization layer for Fluent.
        .package(url: "https://github.com/vapor/auth.git", from: "2.0.4"),
        // 🔑 Hashing (BCrypt, SHA2, HMAC), encryption (AES), public-key (RSA), and random data generation.
        .package(url: "https://github.com/vapor/crypto.git", from: "3.3.3"), 
        // 🔏 JSON Web Token signing and verification (HMAC, RSA).
        .package(url: "https://github.com/vapor/jwt.git", from: "3.1.1"),
        // ✅ Extensible data validation library (name, email, etc)
        .package(url: "https://github.com/vapor/validation.git", from: "2.1.1"),
        // 🔌 Non-blocking, event-driven WebSocket client and server built on Swift NIO.
        //.package(url: "https://github.com/vapor/websocket.git", from: "1.1.2"),
        .package(url: "https://github.com/vapor/leaf.git", from: "3.0.0"),
        // ⚙️ A collection of Swift extensions for wide range of Vapor data types and classes.
        .package(url: "https://github.com/vapor-community/vapor-ext.git", from: "0.3.4"),
        .package(url: "https://github.com/qi-shun-wang/Zip.git", from: "1.2.0"),
        .package(url: "https://github.com/qi-shun-wang/PythonKit.git", .branch("master")),
    ],
    targets: [
        .target(name: "App", dependencies: [
            "Vapor"
            , "VaporExt"
            , "FluentPostgreSQL" 
            , "Validation"
            , "Authentication"
            , "JWT"
            , "Crypto"  
            //, "WebSocket"
            , "Zip"
            , "Leaf"
            , "PythonKit"
        ]),
        .target(name: "Run", dependencies: ["App"]),
        .testTarget(name: "AppTests", dependencies: ["App"])
    ]
)
