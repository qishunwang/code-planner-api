import Vapor
import Zip
final class TemplateController: RouteCollection {
    func boot(router: Router) throws {
        let admin = router.grouped("admin")
        
        let auth = Admin.authSessionsMiddleware()
        let protected = Admin.guardAuthMiddleware()
        admin
            .grouped(auth)
            .grouped(protected)
            .post("template", use: generate)
        admin
            .grouped(auth)
            .grouped(protected)
            .get("template",UUID.parameter, use: select)
    }
    
    func generate(_ req: Request) throws -> Future<TemplateGenerateResponse> {
        let admin = try req.requireAuthenticated(Admin.self)
        let admin_id = try admin.requireID()
        let formFuture = try req.content.decode(TemplateGenerateRequest.self)
        let modulesFuture = formFuture.flatMap { (form) -> EventLoopFuture<([Module], String)> in
            let ids = try form.ids.components(separatedBy: CharacterSet(charactersIn: ","))
                .map { obj -> Int in
                    guard let id = Int(obj) else {throw Abort(.badRequest)}
                    return id
            }
            let builder = try admin.modules.query(on: req)
            
            return builder.group(.or, closure: { (or) in
                for id in ids {
                    or.filter(\.id, .equal, id)
                }
            }).all().map { (m) in
                return (m, form.package)
            }
        }
        let response = modulesFuture.flatMap { (modules, package) -> Future<TemplateGenerateResponse> in
            let random = UUID().uuidString
            let root = try req.make(DirectoryConfig.self).workDir + "Public"
            let location = root + "/\(admin_id)/\(random)/templates"
            let acrchivedLocation = location + "/archived.zip"
            let acrchivedLocationPath = "/\(admin_id)/\(random)/templates/archived.zip"
            
            let module_names = modules.map(){ $0.name }
            try SwiftTemplate.createCoreBase(at: location)
            try SwiftTemplate.createCoreUI(at: location)
            try SwiftTemplate.createAppAssembler(at: location, module_names)
            
            try JavaTemplate.createCoreBase(at: location, package: package)
            try JavaTemplate.createCoreUI(at: location, package: package)
            try JavaTemplate.createAppAssembler(at: location, package: package, module_names)
            
            try KotlinTemplate.createCoreBase(at: location, package: package)
            try KotlinTemplate.createCoreUI(at: location, package: package)
            try KotlinTemplate.createAppAssembler(at: location, package: package, module_names)
            
            try modules.forEach { (module) in
                try SwiftTemplate.createSwiftTemplates(at: location, module_name: module.name)
                try JavaTemplate.createJavaTemplates(at: location, package: package, module_name: module.name)
                try KotlinTemplate.createKotlinTemplates(at: location, package: package, module_name: module.name)
            }
            
            let folderURL = URL(fileURLWithPath: location , isDirectory: true)
            let destinationURL = try Zip.quickZipFiles([folderURL], fileName: random)
            let locationUrl = URL(fileURLWithPath: acrchivedLocation, isDirectory: true)
            try FileManager.default.copyItem(at: destinationURL,
                                             to: locationUrl)
            
            try FileManager.default.removeItem(at: destinationURL)
            return AdminGenerateRecord(location: acrchivedLocationPath, admin_id: admin_id)
                .save(on: req)
                .map { (record) -> (TemplateGenerateResponse) in
                    return try TemplateGenerateResponse(token: record.requireID().uuidString)
            }
        }
        return response
    }
    
    
    func select(_ req: Request) throws -> Future<Response> {
        let admin = try req.requireAuthenticated(Admin.self)
        let id = try req.parameters.next(UUID.self)
        return try admin.generateRecords
            .query(on: req)
            .filter(\.id, .equal, id)
            .sort(\.createdAt, .descending)
            .first()
            .map { (result)  in
                guard let record = result else { throw Abort(.badRequest)}
                return req.redirect(to: record.location)
        }
    }
}

struct TemplateContext: Encodable {
    var module_name: String
}

