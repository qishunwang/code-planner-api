 
 import JWT
 import Authentication
 import Leaf
 /// Controls basic CRUD operations on `Actor`s.
 final class AdminController: RouteCollection {
    func boot(router: Router) throws {
        let admin = router.grouped("admin")
        let auth = Admin.authSessionsMiddleware()
        let protected = Admin.guardAuthMiddleware()
        admin.post("reset", use: reset)
        admin.post("forgot", use: forgot)
        admin.post("registry", use: registry)
        admin.post("signin", use: signin)
        admin.post("verify", use: verify)
        admin
            .grouped(auth)
            .grouped(protected)
            .get("signout", use: signout)
        admin
            .grouped(auth)
            .grouped(protected)
            .get("info",use: info)
        
        admin
            .grouped(auth)
            .grouped(protected)
            .put("password", use: updatePassword)
        
        admin
            .grouped(auth)
            .grouped(protected)
            .post(use: create)
        
        admin
            .grouped(auth)
            .grouped(protected)
            .get("admins",use: index)
        
    }
    static func encryptToken(username:String,password:String) throws -> Data {
        let payload = RegistryPayload(sub: "admin_registry",
                                      username: username,
                                      password: password,
                                      admin: true,
                                      exp: .init(value: .init(timeInterval: 60, since: Date()))
        )
        let jwt = JWT(payload: payload)
        let data = try jwt.sign(using: JWTSigner.hs256(key: Data("cp-hs256".utf8)))
        return data
    }
    static func decryptToken(_ token: String) throws -> JWT<RegistryPayload> {
        guard let data = token.data(using: .utf8) else { throw Abort(.badRequest) }
        let signer = JWTSigner.hs256(key: Data("cp-hs256".utf8))
        return try JWT<RegistryPayload>(from: data, verifiedUsing: signer)
    }
    
    static func hashedPassword(_ password: String) throws -> String{
        return try BCrypt.hash(password)
    }
    
    func verify(_ req: Request) throws -> Future<HTTPResponse> {
        return try req.content.decode(AdminVerifyRequest.self)
            .flatMap{ (form)  in
                var payload:RegistryPayload
                var username:String
                var password:String
                
                do {
                    payload = try AdminController.decryptToken(form.token).payload
                    try payload.exp.verifyNotExpired()
                    username = payload.username
                    password = try AdminController.hashedPassword(payload.password)
                } catch {
                    throw Abort(.forbidden)
                }
                
                
                let responseFuture = req.transaction(on: .psql) { (conn) -> EventLoopFuture<HTTPResponse> in
                    return Admin.findOne(by: [\.username == username], on: conn)
                        .map { result in
                            if result != nil {
                                return HTTPResponse(status: .forbidden)
                            } else {
                                let _ = Admin(username: username, password: password).save(on: conn)
                                return HTTPResponse(status: .ok)
                            }
                    }
                }
                return responseFuture
        }
    }
    func reset(_ req: Request) throws -> Future<HTTPResponse> {
        return try req.content.decode(AdminResetRequest.self)
            .flatMap{ (form)  in
                var payload:RegistryPayload
                var username:String
                var password:String
                
                do {
                    payload = try AdminController.decryptToken(form.token).payload
                    try payload.exp.verifyNotExpired()
                    username = payload.username
                    password = try AdminController.hashedPassword(form.password)
                } catch {
                    throw Abort(.forbidden)
                }
                
                let responseFuture = req.transaction(on: .psql) { (conn) -> EventLoopFuture<HTTPResponse> in
                    return Admin.findOne(by: [\.username == username], on: conn)
                        .map { result in
                            if let admin = result {
                                admin.password = password
                                let _ = admin.save(on: conn)
                                return HTTPResponse(status: .ok)
                            } else {
                                return HTTPResponse(status: .forbidden)
                            }
                    }
                }
                return responseFuture
        }
    }
    
    func forgot(_ req: Request) throws -> Future<HTTPResponse> {
        return try req.content.decode(AdminForgotRequest.self)
            .flatMap{ (form)  in
                let isExistFuture = Admin.findOne(by: [\.username == form.username], on: req)
                    .map { (result) -> Bool in
                        guard let admin = result else { return false }
                        let data = try AdminController.encryptToken(username: admin.username, password: admin.password)
                        let token = String(data: data, encoding: .utf8)
                        let url = Environment.get(AppEnvironment.VERIFY_WEB_URL.value) ?? "http://localhost:8081"
                        
                        
                        let plainText = """
                        Hello,
                        Thank you for using Code Planner! Please click the button below to reset your password.
                        Note: If you did not create a Code Planner account, please just ignore this email.
                        – Code Planner https://cp.routine.space
                        
                        Reset Link: \(url)/forgot?token=\(token ?? "")
                        """
                        let leaf = try req.make(LeafRenderer.self)
                        let templateData = EmailTemplateData(verify_url: "\(url)/forgot?token=\(token ?? "")",
                            help_center_url: url,
                            header_text: "Oops!",
                            content_text: "Thank you for using Code Planner! Please click the button below to reset your password.",
                            main_button_text: "Reset My Password")
                        let _ = leaf.render("email", templateData)
                            .map { view in
                                let mail = Email(fromName: "Code Planner",
                                                 fromEmail: nil,
                                                 to: form.username,
                                                 subject: "Forgot Password from Code Planner",
                                                 text: plainText,
                                                 html: String(data: view.data, encoding:.utf8) ?? ""
                                )
                                let _ = try req.make(EmailDeliveryService.self).send(mail, on: req)
                        }
                        return true
                }
                return isExistFuture.map { (isExist)  in
                    if isExist {
                        return HTTPResponse(status: .ok)
                    } else {
                        return HTTPResponse(status: .forbidden)
                    }
                }
        }
    }
    
    func registry(_ req: Request) throws -> Future<HTTPResponse> {
        return try req.content.decode(AdminRegistryRequest.self)
            .flatMap { form  in
                try Admin(username: form.username, password: form.password).validate()
                let isExistFuture = req.transaction(on: .psql) { (conn) -> EventLoopFuture<Bool> in
                    return Admin.findOne(by: [\.username == form.username], on: conn)
                        .map { (result) -> (Bool) in
                            if result == nil {
                                let data = try AdminController.encryptToken(username: form.username, password: form.password)
                                let token = String(data: data, encoding: .utf8)
                                let url = Environment.get(AppEnvironment.VERIFY_WEB_URL.value) ?? "http://localhost:8081"
                                
                                
                                let plainText = """
                                Hello,
                                Thank you for signing up for Code Planner! Please click the button below to confirm your email address:
                                Confirm sign up
                                Note: If you did not create a Code Planner account, please just ignore this email.
                                – Code Planner https://cp.routine.space
                                
                                Confirm Link: \(url)/verify?token=\(token ?? "")
                                """
                                let leaf = try req.make(LeafRenderer.self)
                                //                                Thank you!
                                //                                Thanks for joining Code Planner. just click to confirm your email address to receive our daily email and get access to our platform.
                                //                                CONFIRM ACCOUNT
                                
                                let templateData = EmailTemplateData(verify_url: "\(url)/verify?token=\(token ?? "")",
                                    help_center_url: url,
                                    header_text: "Thank you!",
                                    content_text: "Thanks for joining Code Planner. just click to confirm your email address to receive our daily email and get access to our platform.",
                                    main_button_text: "CONFIRM ACCOUNT")
                                let _ = leaf.render("email", templateData)
                                    .map { view in
                                        let mail = Email(fromName: "Code Planner",
                                                         fromEmail: nil,
                                                         to: form.username,
                                                         subject: "Confirm email address for Code Planner",
                                                         text: plainText,
                                                         html: String(data: view.data, encoding:.utf8) ?? ""
                                        )
                                        let _ = try req.make(EmailDeliveryService.self).send(mail, on: req)
                                }
                                
                            }
                            return result != nil
                    }
                }
                return isExistFuture.map { (isExist) in
                    if isExist {
                        return HTTPResponse(status: .forbidden)
                    } else {
                        return HTTPResponse(status: .ok)
                    }
                }
        }
    }
    
    func signin(_ req: Request) throws -> Future<HTTPResponse> {
        return try req.content.decode(AdminSignInRequest.self)
            .flatMap { form  in
                return Admin.authenticate(username: form.username, password: form.password, using: BCrypt.self, on: req)
                    .map {  admin in
                        guard let admin = admin else { throw Abort(.unauthorized, reason: "Wrong Password or Username.")}
                        try req.authenticateSession(admin)
                        return HTTPResponse(status: .ok)
                }
        }
    }
    
    func signout(_ req: Request) throws -> Future<HTTPResponse> {
        guard let key = try req.session().id else {throw Abort(.unauthorized) }
        try req.unauthenticateSession(Admin.self)
        try req.destroySession()
        return try req.keyedCache(for: .psql).remove(key)
            .map {
                return HTTPResponse(status: .ok)
        }
    }
    
    func info(_ req: Request) throws -> Response {
        let admin = try req.requireAuthenticated(Admin.self)
        let response = req.response()
        try response.content.encode(AdminInfoResponse(username: admin.username))
        return response
    }
    
    func updatePassword(_ req: Request) throws -> Future<Response> {
        let admin = try req.requireAuthenticated(Admin.self)
        
        return try req.content.decode(AdminUpdatePasswordRequest.self)
            .flatMap { form in
                let cryptedPassword = try BCryptDigest().hash(form.password.convertToData())
                admin.password = cryptedPassword
                return admin.save(on: req).map { _ in
                    return req.response(http: HTTPResponse(status: HTTPResponseStatus.noContent))
                }
        }
    }
    
    func create(_ req: Request) throws -> Future<HTTPStatus> {
        let formFuture = try req.content.decode(AdminCreateRequest.self)
        return formFuture.flatMap { form in
            let isExistedFuture = Admin.query(on: req, withSoftDeleted: false)
                .filter(\Admin.username == form.username)
                .first()
                .map { result in
                    return result != nil
            }
            return isExistedFuture.flatMap { isExisted in
                if isExisted {throw Abort(.forbidden)}
                let cryptedPassword = try BCryptDigest().hash(form.password.convertToData())
                return Admin(username: form.username,
                             password: cryptedPassword)
                    .save(on: req)
                    .transform(to: HTTPStatus.ok)
            }
        }
    }
    
    
    func index(_ req: Request) throws -> Future<AdminListResponse> {
        let criteria: [FilterOperator<Admin.Database, Admin>] =  []
        var sort: [Admin.Database.QuerySort] = []
        let form = try req.query.decode(QueryAdminLIstRequest.self)
        let lower = form.lower ?? 0
        let upper = form.upper ?? (lower + 100)
        
        let defaultSort = Admin.Database.querySort(Admin.Database.queryField(.keyPath(\Admin.createdAt)), .descending)
        sort.append(defaultSort)
        let baseQuery = Admin.query(by: criteria, on: req, withSoftDeleted: false)
        let totalFuture = baseQuery.count()
        return totalFuture.flatMap { total in
            let adminFuture = baseQuery
                .sort(by: sort)
                .range(lower: lower, upper: upper)
                .all()
                .map { (admins) -> ([Admin.Public]) in
                    return admins.map { admin in return admin.public()}
            }
            return adminFuture.map { (admins) in
                return AdminListResponse(admins: admins, lower: lower, upper: upper, total: total)
            }
        }
    }
 }
 
 
 
 struct RegistryPayload: JWTPayload {
    var sub: SubjectClaim
    var username: String
    var password: String
    var admin: Bool
    var exp: ExpirationClaim
    
    func verify(using signer: JWTSigner) throws {
        try exp.verifyNotExpired()
    }
 }
 
 struct EmailTemplateData: Content {
    let verify_url: String
    let help_center_url: String
    let header_text: String
    let content_text: String
    let main_button_text: String
 }
