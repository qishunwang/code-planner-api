import Vapor

final class ProjectController: RouteCollection {
    func boot(router: Router) throws {
        let admin = router.grouped("admin")
        
        let auth = Admin.authSessionsMiddleware()
        let protected = Admin.guardAuthMiddleware()
        admin
            .grouped(auth)
            .grouped(protected)
            .post("projects", use: create)
        
        admin
            .grouped(auth)
            .grouped(protected)
            .get("projects", use: index)
    }
    
    func index(_ req: Request) throws -> EventLoopFuture<ProjectListResponse> {
        let admin = try req.requireAuthenticated(Admin.self)
        let params = try req.query.decode(ProjectListRequest.self)
        let lower = params.lower ?? 0
        let upper = params.upper ?? lower + 2
        let query = try admin.projects.query(on: req)
        let count = query.count()
        return count.flatMap { amount  in
            return query
                .range(lower: lower, upper: upper)
                .all()
                .map { (projects)  in
                    
                    return ProjectListResponse(projects: projects, lower: lower, upper: upper, total: amount)
            }
        }
    }
    
    func create(_ req: Request) throws -> EventLoopFuture<HTTPResponse> {
        
        let formFuture = try req.content.decode(ProjectCreateRequest.self)
        let admin = try req.requireAuthenticated(Admin.self)
        
        return formFuture.flatMap { form in
            return req.transaction(on: .psql) { (conn)  in
                return Project(name: form.name, packageName: form.packageName)
                    .save(on: conn)
                    .map({ (project) in
                        let _ = try AdminProject(admin_id: admin.requireID(), project_id: project.requireID()).save(on: conn)
                        return HTTPResponse(status: .ok)
                    })
            }
        }
    }
    
}
