import Vapor

final class ModuleController: RouteCollection {
    func boot(router: Router) throws {
        let admin = router.grouped("admin")
        
        let auth = Admin.authSessionsMiddleware()
        let protected = Admin.guardAuthMiddleware()
        admin
            .grouped(auth)
            .grouped(protected)
            .post("projects", Project.parameter, "modules", use: create)
        
        admin
            .grouped(auth)
            .grouped(protected)
            .get("projects",Project.parameter, "modules", use: index)
    }
    
    func index(_ req: Request) throws -> EventLoopFuture<ModuleListResponse> {
        let projectFuture = try req.parameters.next(Project.self)
        let admin = try req.requireAuthenticated(Admin.self)
        let params = try req.query.decode(ModuleListRequest.self)
        let lower = params.lower ?? 0
        let upper = params.upper ?? lower + 2
        return projectFuture.flatMap { project in
            return admin.projects.isAttached(project, on: req)
                .flatMap { isOwner in
                    if isOwner {
                        let query = try project.modules.query(on: req)
                        let count = query.count()
                        return count.flatMap { amount  in
                            return query
                                .range(lower: lower, upper: upper)
                                .all()
                                .map { (modules)  in
                                    return ModuleListResponse(modules: modules, lower: lower, upper: upper, total: amount)
                            }
                        }
                    } else {
                        throw Abort(.forbidden)
                    }
            }
        }
    }
    
    func create(_ req: Request) throws -> EventLoopFuture<HTTPResponse> {
        let projectFuture = try req.parameters.next(Project.self)
        let formFuture = try req.content.decode(ModuleCreateRequest.self)
        let admin = try req.requireAuthenticated(Admin.self)
        
        return projectFuture.flatMap { project in
            let isOwnerFuture = admin.projects.isAttached(project, on: req)
            return isOwnerFuture.flatMap { isOwner in
                if isOwner {
                    return formFuture.flatMap { form in
                        return req.transaction(on: .psql) { (conn)  in 
                            return Module(name: form.name).save(on: conn)
                                .map({ (module) in
                                    let _ = try ProjectModule(project_id: project.requireID(), module_id: module.requireID()).save(on: conn)
                                    let _ = try AdminModule(admin_id: admin.requireID(), module_id: module.requireID()).save(on: conn)
                                    return HTTPResponse(status: .ok)
                                })
                        }
                    }
                } else {
                    return req.future(HTTPResponse(status: .unauthorized))
                }
            }
            
        }
        
    }
    
}
