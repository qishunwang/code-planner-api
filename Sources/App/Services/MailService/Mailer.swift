import PythonKit

struct Mailer {
    let config: SMTPConfig
    
    func send(subject: String, fromEmail: String?, with fromName: String? ,to: String, plain: String, html: String) throws{
        let smtplib = Python.import("smtplib")
        let MIMEMultipart = Python.import("email.mime.multipart").MIMEMultipart
//        let MIMEImage = Python.import("email.mime.image").MIMEImage
        let MIMEText = Python.import("email.mime.text").MIMEText
//        let Header = Python.import("email.header").Header
        let utils = Python.import("email.utils")
        let root = try MIMEMultipart("related")
        let alternative = try MIMEMultipart("alternative")
        let plainText = try MIMEText(plain, "plain", "utf-8")
        let htmlText = try MIMEText(html, "html", "utf-8")
        root.preamble = "This is a multi-part message in MIME format."
        
        root["Message-Id"] = try utils.make_msgid()
        root["Date"] = try utils.formatdate()
        root["Subject"] = PythonObject(stringLiteral: subject)
        root["From"] = PythonObject(stringLiteral: "\(fromName ?? config.email)<\(fromEmail ?? config.email)>")
        root["To"] = PythonObject(stringLiteral: "\(to)")
        try alternative.attach(plainText)
        try alternative.attach(htmlText)
        try root.attach(alternative)
        
        let smtp = try smtplib.SMTP(config.hostname, port: 587)
        try smtp.starttls()
        try smtp.login(config.email, config.password)
        try smtp.set_debuglevel(1)
        try smtp.sendmail(fromEmail ?? config.email, to, root.as_string())
        try smtp.quit()
    }
}
