import Service

protocol EmailDeliveryService: Service {
    func send(_ message: Email, on container: Container) -> EventLoopFuture<String>
}


struct SMTPProvider: Provider {
    private let config: SMTPConfig
    
    init(config: SMTPConfig) {
        self.config = config
    }
    
    func register(_ services: inout Services) throws {
        let mailer = Mailer(config: config)
        services.register(mailer, as: EmailDeliveryService.self)
    }
    
    func didBoot(_ container: Container) throws -> EventLoopFuture<Void> {
        return .done(on: container)
    }
}

//extension PythonObject: Service {}
extension Mailer: EmailDeliveryService {
    func send(_ message: Email, on container: Container) -> EventLoopFuture<String> {
        do {
            try send(subject: message.subject,
                     fromEmail: message.fromEmail,
                     with: message.fromName,
                     to: message.to,
                     plain: message.text,
                     html: message.html)
            return container.future("skip")
        } catch let err {
            return container.future(err.localizedDescription)//promise.futureResult
        }
        
    }
}
