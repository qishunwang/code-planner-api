struct Email {
    let fromName: String?
    let fromEmail: String?
    let to: String
    let subject: String
    let text: String
    let html: String
}
