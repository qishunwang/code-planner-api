import FluentPostgreSQL
import Crypto
struct AdminMigration: Migration {
    static func revert(on conn: PostgreSQLConnection) -> EventLoopFuture<Void> {
        return Admin.revert(on: conn)
    }
    
    typealias Database = PostgreSQLDatabase
    
    static func prepare(on conn: AdminMigration.Database.Connection) -> EventLoopFuture<Void> {
        let password = try? BCrypt.hash("password")
        guard let passwordHashed = password else {
            fatalError("Failed to create admin user")
        }
        let admin = Admin(username: "admin", password: passwordHashed)
         
       return admin.save(on: conn).transform(to: ())
    }
}
