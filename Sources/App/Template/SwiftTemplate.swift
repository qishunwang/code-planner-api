import Foundation
final class SwiftTemplate {
    
    //MARK: Module Assembler & AppDependencies
    public static func createAppAssembler(at root: String, _ modules: [String]) throws {
        typealias Template = SwiftTemplate
        
        let folder = root + "/ios/"
        var file = folder + "AppModuleAssembler.swift"
        var data = Template.createAppModuleAssembler(modules).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
         
        file = folder + "AppDependencies.swift"
        data = Template.createAppDependencies(modules).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
        file = folder + "RootViewController.swift"
        data = Template.createRoot().data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
    }
    
    private static func createRoot() -> String {
        return """
        import UIKit

        class RootViewController: UIViewController {
            weak var dependency: AppDependencies!
            
            convenience init(dependency: AppDependencies) {
                self.init()
                self.dependency = dependency
            }
        
            override func viewDidLoad() {
                super.viewDidLoad()
                dependency.installRootView(root: view)
            }
            
            override func viewDidDisappear(_ animated: Bool) {
                super.viewDidDisappear(animated)
                dependency.uninstallRootView()
            }

            override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
                super.viewWillTransition(to: size, with: coordinator)
                print("viewWillTransition:\\(size)")
                print("=>coordinator:\\(coordinator)")
                view.subviews.forEach{
                    $0.viewWillTransition(to: size)
                }
            }
        }
        """
    }
    
    private static func createAppDependencies(_ modules: [String]) -> String {
        var configures: String = ""
        var presentations: String = ""
        var dismissals: String = ""
        var setupManagers: String = ""
        var setupModules: String = ""
        for (i, module) in modules.enumerated() {
            configures.append("\t\tassembler.\(module.camelized)Wireframe.configure(parent: root)\n")
            presentations.append("\t\t\(i == 0 ? "" : "//")assembler.\(module.camelized)Wireframe.present\(module.trimed)ViewFromParent()\n")
            dismissals.append("\t\t\(i == 0 ? "" : "//")assembler.\(module.camelized)Wireframe.dismiss\(module.trimed)ViewFromParent()\n")
            setupManagers.append("\t\tlet \(module.camelized)DataManager = \(module.trimed)DataManager()\n")
            setupModules.append("\t\tassembler.setup\(module.trimed)Module(\(module.camelized)DataManager: \(module.camelized)DataManager)\n")
        }
        return """
        import Foundation
        import UIKit
        
        /*
        @UIApplicationMain
        class AppDelegate: UIResponder, UIApplicationDelegate {
            
            var window: UIWindow?
            let appDependencies = AppDependencies()
            var rootViewController: RootViewController?

            func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
                // Override point for customization after application launch.
                window = UIWindow(frame: UIScreen.main.bounds)
                rootViewController = RootViewController(dependency: appDependencies)
                if #available(iOS 13.0, *) {
                    
                } else {
                    window?.rootViewController = rootViewController
                    window?.makeKeyAndVisible()
                }
                return true
            }

            @available(iOS 13.0, *)
            func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
                // Called when a new scene session is being created.
                // Use this method to select a configuration to create the new scene with.
                return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
            }
            
            @available(iOS 13.0, *)
            func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
                // Called when the user discards a scene session.
                // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
                // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
            }
        }

        class SceneDelegate: UIResponder, UIWindowSceneDelegate {
            
            var window: UIWindow?
            
            @available(iOS 13.0, *)
            func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
                // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
                // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
                // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
                guard let windowScene = (scene as? UIWindowScene) else { return }
                window = UIWindow(windowScene: windowScene)
                window?.rootViewController = (UIApplication.shared.delegate as! AppDelegate).rootViewController
                window?.makeKeyAndVisible()
            }
            
            @available(iOS 13.0, *)
            func sceneDidDisconnect(_ scene: UIScene) {
                // Called as the scene is being released by the system.
                // This occurs shortly after the scene enters the background, or when its session is discarded.
                // Release any resources associated with this scene that can be re-created the next time the scene connects.
                // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
            }
            
            @available(iOS 13.0, *)
            func sceneDidBecomeActive(_ scene: UIScene) {
                // Called when the scene has moved from an inactive state to an active state.
                // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
            }
            
            @available(iOS 13.0, *)
            func sceneWillResignActive(_ scene: UIScene) {
                // Called when the scene will move from an active state to an inactive state.
                // This may occur due to temporary interruptions (ex. an incoming phone call).
            }
            
            @available(iOS 13.0, *)
            func sceneWillEnterForeground(_ scene: UIScene) {
                // Called as the scene transitions from the background to the foreground.
                // Use this method to undo the changes made on entering the background.
            }
            
            @available(iOS 13.0, *)
            func sceneDidEnterBackground(_ scene: UIScene) {
                // Called as the scene transitions from the foreground to the background.
                // Use this method to save data, release shared resources, and store enough scene-specific state information
                // to restore the scene back to its current state.
            }
        }


         */
        final class AppDependencies {
            
            private let assembler = AppModuleAssembler()
            
            init() {
                configureDependencies()
            }
            
            func installRootView(root: UIView) {
        \(configures)
        \(presentations)
                
            }
            
            func uninstallRootView() {
        \(dismissals)
            }
            
            func configureDependencies() {
        \(setupManagers)
        \(setupModules)
            }
        }


        """
    }
    private static func createAppModuleAssembler(_ modules: [String]) -> String {
        var presenters: String = ""
        var wireframes: String = ""
        var setupModules: String = ""
        for module in modules {
            presenters.append("\tprivate let \(module.camelized)Presenter = \(module.trimed)Presenter()\n")
            wireframes.append("\tlet \(module.camelized)Wireframe = \(module.trimed)Wireframe()\n")
            setupModules.append("""
                    // MARK: Setup \(module) Module
                    func setup\(module.trimed)Module(\(module.camelized)DataManager: \(module.trimed)DataManager) {
                        let \(module.camelized)Interator = \(module.trimed)Interactor(dataManager: \(module.camelized)DataManager)
                        
                        \(module.camelized)Interator.configure(output: \(module.camelized)Presenter)
                        \(module.camelized)Presenter.configure(interactor: \(module.camelized)Interator)
                        \(module.camelized)Presenter.configure(router: \(module.camelized)Wireframe)
                        \(module.camelized)Wireframe.configure(presenter: \(module.camelized)Presenter)
                    }
                
                """)
        }
        return """
        import Foundation

        final class AppModuleAssembler {
        \(presenters)
        \(wireframes)
        \(setupModules)
        }

        """
    }
    
    //MARK:MI
    public static func createModuleDelegate(name:String) -> String {
        return """
        protocol \(name.trimed)ModuleDelegate {
        
        }
        
        """
    }
    
    public static func createModuleInterface(name:String) -> String {
        return """
        protocol \(name.trimed)ModuleInterface {
        
        }

        """
    }
    //MARK:UI
    /// View
    public static func createView(name:String) -> String {
        return """
        import UIKit

        class \(name.trimed)View: UIView {
            
            var eventHandler: \(name.trimed)ModuleInterface?
            var transitionDelegate: ViewTransitionDelegate?
            private var container: UIView!
            private var floatContainer: UIView!
            private var portraitSet:[NSLayoutConstraint] = []
            private var landscapeSet:[NSLayoutConstraint] = []

            required init?(coder aDecoder: NSCoder) {
                super.init(coder: aDecoder)
                initChildView()
            }
            
            override init(frame: CGRect) {
                super.init(frame: frame)
                initChildView()
            }
            
            func initChildView(){
                backgroundColor = .white
                container = UIView()
                container.translatesAutoresizingMaskIntoConstraints = false
                container.backgroundColor = .blue
                
                floatContainer = UIView()
                floatContainer.translatesAutoresizingMaskIntoConstraints = false
                floatContainer.backgroundColor = .red
                
                addSubview(container)
                addSubview(floatContainer)
                
                portraitSet.append(contentsOf: [
                    container.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
                    container.leftAnchor.constraint(equalTo: leftAnchor,constant: 16),
                    container.rightAnchor.constraint(equalTo: rightAnchor,constant: -16),
                    container.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -16),
                    
                    floatContainer.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
                    floatContainer.leftAnchor.constraint(equalTo: leftAnchor,constant: 16),
                    floatContainer.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1/3),
                    floatContainer.heightAnchor.constraint(equalTo: floatContainer.widthAnchor, multiplier: 4/3),
                ])
                
                landscapeSet.append(contentsOf: [
                    container.topAnchor.constraint(equalTo: topAnchor,constant: 16),
                    container.leftAnchor.constraint(equalTo: safeAreaLayoutGuide.leftAnchor),
                    container.rightAnchor.constraint(equalTo: safeAreaLayoutGuide.rightAnchor),
                    container.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16),
                    
                    floatContainer.topAnchor.constraint(equalTo:  topAnchor,constant: 16),
                    floatContainer.leftAnchor.constraint(equalTo: safeAreaLayoutGuide.leftAnchor),
                    floatContainer.widthAnchor.constraint(equalTo: heightAnchor, multiplier: 1/3),
                    floatContainer.heightAnchor.constraint(equalTo: floatContainer.widthAnchor, multiplier: 4/3),
                ])

            }

            override func layoutSubviews() {
                super.layoutSubviews()
                print("layoutSubviews")
                let isPortrait = frame.size.height > frame.size.width
                NSLayoutConstraint.deactivate(isPortrait ? landscapeSet: portraitSet)
                NSLayoutConstraint.activate(isPortrait ? portraitSet: landscapeSet)
            }
            
            override func viewWillTransition(to size: CGSize) {
                super.viewWillTransition(to: size)
                frame.size = size
            }
            
            deinit {
                print("deinit-->\\(self)")
            }

        }

        extension \(name.trimed)View: \(name.trimed)ViewInterface {
            
            func present() {
                transitionDelegate?.shouldAttachToSuperview()
            }
            
            func dismiss() {
                transitionDelegate?.shouldRemoveFromSuperview()
            }
            
        }

        """
    }
    
    public static func createViewInterface(name:String) -> String {
        return """
        protocol \(name.trimed)ViewInterface: BaseView {
            func present()
            func dismiss()
        }

        """
    }
    /// wireframe
    public static func createWireframeInterface(name:String) -> String {
        return """
        protocol \(name.trimed)WireframeInterface {
            func present\(name.trimed)ViewFromParent()
            func dismiss\(name.trimed)ViewFromParent()
        }
        
        """
    }
    
    public static func createWireframe(name:String) -> String {
        return """
        import UIKit

        class \(name.trimed)Wireframe : BaseRouting {
            typealias Presenter = (\(name.trimed)PresenterInterface&\(name.trimed)ModuleInterface)
            
            typealias ParentView = UIView
            private var presenter: Presenter?
            private weak var parent: ParentView!
            private var \(name.camelized)View: \(name.trimed)View!
            
            func configure(parent: ParentView) {
                self.parent = parent
            }
            
            func configure(presenter: Presenter) {
                self.presenter = presenter
            }
            
            func presentViewFromParent() {
                if \(name.camelized)View != nil {return} // lock the duplicate action
                let frame = parent.frame
                \(name.camelized)View = \(name.trimed)View(frame: frame)
                \(name.camelized)View.transitionDelegate = self
                \(name.camelized)View.eventHandler = presenter
                
                parent.addSubview(\(name.camelized)View)
                presenter?.attachView(view: \(name.camelized)View)
            }
            
            func dismissViewFromParent() {
                presenter?.detachView()
            }
            
        }

        extension \(name.trimed)Wireframe: \(name.trimed)WireframeInterface {
            func present\(name.trimed)ViewFromParent(){
                presentViewFromParent()
            }
            
            func dismiss\(name.trimed)ViewFromParent() {
                dismissViewFromParent()
            }
        }

        
        extension \(name.trimed)Wireframe: ViewTransitionDelegate {
            func shouldAttachToSuperview() {
            PresentationTransition.animate(view: \(name.camelized)View, with: parent.frame) {
                    
                }
            }
            
            func shouldRemoveFromSuperview() {
                DismissalTransition.animate(view: \(name.camelized)View, with: parent.frame) {
                    self.\(name.camelized)View.removeFromSuperview()
                    self.\(name.camelized)View.transitionDelegate = nil
                    self.\(name.camelized)View.eventHandler = nil
                    self.\(name.camelized)View = nil
                }
            }
        }

        """
    }
    
    // presenter
    public static func createPresenterInterface(name:String) -> String {
        return """
        protocol \(name.trimed)PresenterInterface {
            func attachView(view: \(name.trimed)ViewInterface)
            func detachView()
        }
        
        """
    }
    
    public static func createPresenter(name:String) -> String {
        return """
        import Foundation

        class \(name.trimed)Presenter {
            
            private var moduleDelegate: ModuleDelegate?
            private var view: View?
            private var interactor: Interactor?
            private var router: Router?
        
        }

        extension \(name.trimed)Presenter: BaseModulePresenter {
            
            typealias ModuleDelegate = \(name.trimed)ModuleDelegate
            typealias View = \(name.trimed)ViewInterface
            typealias Interactor = \(name.trimed)InteractorInput
            typealias Router = \(name.trimed)WireframeInterface
            
            func configure(interactor: Interactor) {
                self.interactor = interactor
            }
            
            func configure(router: Router) {
                self.router = router
            }
            
            func configure(moduleDelegate: ModuleDelegate) {
                self.moduleDelegate = moduleDelegate
            }
        }

        extension \(name.trimed)Presenter: \(name.trimed)PresenterInterface {
            func attachView(view: \(name.trimed)ViewInterface) {
                self.view = view
                self.view?.present()
            }
            
            func detachView() {
                view?.dismiss()
                view = nil

            }
        }


        extension \(name.trimed)Presenter: \(name.trimed)ModuleInterface {
           
           
        }

        extension \(name.trimed)Presenter: \(name.trimed)InteractorOutput {
             
        }

        
        """
    }
    // interactor
    public static func createInteractorInput(name:String) -> String {
        return """
        protocol \(name.trimed)InteractorInput: BaseInteractorInput {
        
        }
        
        """
    }
    public static func createInteractorOutput(name:String) -> String {
        return """
        protocol \(name.trimed)InteractorOutput: BaseInteractorOutput {
            
        }
        
        """
    }

    public static func createInteractor(name:String) -> String {
        return """
        class \(name.trimed)Interactor: BaseInteractor {
            
            typealias Output = \(name.trimed)InteractorOutput
            
            private var output: \(name.trimed)InteractorOutput?
            
            private var \(name.camelized)DataManager: \(name.trimed)DataManager
            
            init(dataManager: \(name.trimed)DataManager) {
                \(name.camelized)DataManager = dataManager
            }
        }

        extension \(name.trimed)Interactor: \(name.trimed)InteractorInput {
            
            func configure(output: \(name.trimed)InteractorOutput) {
                self.output = output
            }
            
        }

        """
    }
    
    public static func createDataManager(name:String) -> String {
       return """
       class \(name.trimed)DataManager {
           
       }

       """
    }
    public static func createSwiftTemplates(at root: String, module_name: String) throws {
        typealias Template = SwiftTemplate
        var folder = root + "/ios/modules/\(module_name.lowercasedTrimed)/mi/"
        var file = folder + "\(module_name.trimed)ModuleDelegate.swift"
        var data = Template.createModuleDelegate(name: module_name).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
        file = folder + "\(module_name.trimed)ModuleInterface.swift"
        data = Template.createModuleInterface(name: module_name).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
        
        folder = root + "/ios/modules/\(module_name.lowercasedTrimed)/ui/presenter/"
        file = folder + "\(module_name.trimed)PresenterInterface.swift"
        data = Template.createPresenterInterface(name: module_name).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
        file = folder + "\(module_name.trimed)Presenter.swift"
        data = Template.createPresenter(name: module_name).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
        
        folder = root + "/ios/modules/\(module_name.lowercasedTrimed)/ui/wireframe/"
        file = folder + "\(module_name.trimed)Wireframe.swift"
        data = Template.createWireframe(name: module_name).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
        file = folder + "\(module_name.trimed)WireframeInterface.swift"
        data = Template.createWireframeInterface(name: module_name).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
        
        folder = root + "/ios/modules/\(module_name.lowercasedTrimed)/ui/view/"
        file = folder + "\(module_name.trimed)ViewInterface.swift"
        data = Template.createViewInterface(name: module_name).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
        file = folder + "\(module_name.trimed)View.swift"
        data = Template.createView(name: module_name).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
        folder = root + "/ios/modules/\(module_name.lowercasedTrimed)/logic/interactor/"
        file = folder + "\(module_name.trimed)Interactor.swift"
        data = Template.createInteractor(name: module_name).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
        file = folder + "\(module_name.trimed)InteractorInput.swift"
        data = Template.createInteractorInput(name: module_name).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
        file = folder + "\(module_name.trimed)InteractorOutput.swift"
        data = Template.createInteractorOutput(name: module_name).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
        
        folder = root + "/ios/modules/\(module_name.lowercasedTrimed)/logic/manager/"
        file = folder + "\(module_name.trimed)DataManager.swift"
        data = Template.createDataManager(name: module_name).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
    }
    //MARK:Core UI
    public static func createCoreUI(at root: String) throws {
        typealias Template = SwiftTemplate

        let folder = root + "/ios/core/ui/"
        var file = folder + "ViewTransitionDelegate.swift"
        var data = Template.createViewTransitionDelegate().data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "DismissalTransition.swift"
        data = Template.createDismissalTransition().data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "PresentationTransition.swift"
        data = Template.createPresentationTransition().data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
        file = folder + "UIView+Extension.swift"
        data = Template.createUIViewExtension().data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
    }
    
    public static func createUIViewExtension() -> String {
        return """
        #if os(iOS)
        import UIKit
        import Foundation

        extension UIView: BaseView {
            public func run(action: @escaping UIView.Main) {
                DispatchQueue.main.async{
                    action()
                }
            }
        }

        extension UIView {
           @objc public func viewWillTransition(to size: CGSize){
                print("\\(self):[\\(self.frame)]====>[\\(size)]")
            }
        }

        #endif
        """
    }
    public static func createViewTransitionDelegate() -> String {
        return """
        protocol ViewTransitionDelegate {
            func shouldAttachToSuperview()
            func shouldRemoveFromSuperview()
        }

        """
    }
    
    public static func createDismissalTransition() -> String {
        return """
        import Foundation
        import UIKit

        class DismissalTransition {
            
            typealias Result = () -> Void
            
            static func animate(view: UIView, with area: CGRect, done: @escaping Result) {
                UIView.animate(withDuration: 2, animations: {
                    view.frame.origin = CGPoint(x:0, y: -area.size.height)
                }, completion: { (finished) in
                    if (finished){
                        done()
                    }
                })
            }
        }
        """
    }
    
    public static func createPresentationTransition() -> String {
        return """
        import Foundation
        import UIKit

        class PresentationTransition {
            
            typealias Result = () -> Void
            
            static func animate(view: UIView, with area: CGRect, done: @escaping Result) {
                view.frame.origin = CGPoint(x: -area.size.width, y: 0)
                UIView.animate(withDuration: 0.2, animations: {
                    view.frame.origin = CGPoint(x:0, y: 0)
                }, completion: { (finished) in
                    if (finished){
                        done()
                    }
                })
            }
        }
       """
    }
    
    //MARK:Core Base
    public static func createCoreBase(at root: String) throws {
       typealias Template = SwiftTemplate
        
        let folder = root + "/ios/core/base/"
        var file = folder + "BaseModulePresenter.swift"
        var data = Template.createBaseModulePresenter().data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
        file = folder + "BaseRouting.swift"
        data = Template.createBaseRouting().data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "BaseEntity.swift"
        data = Template.createBaseEntity().data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
        file = folder + "BasePresenter.swift"
        data = Template.createBasePresenter().data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
        file = folder + "BaseInteractor.swift"
        data = Template.createBaseInteractor().data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
        file = folder + "BaseView.swift"
        data = Template.createBaseView().data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
    }
     
    public static func createBaseModulePresenter() -> String {
      
        return """
        public protocol BaseModulePresenter: BasePresenter {
            associatedtype ModuleDelegate
            
            func configure(moduleDelegate: ModuleDelegate)
        }
        """
     }
     
    public static func createBaseRouting() -> String {
      
        return """
        /**
         * Routing contains navigation logic for describing which screens are shown in which order.
         *
         * Routes from one screen to another are defined in the wireframes created by an interaction designer.
         * In VIPER, the responsibility for Routing is shared between two objects: the Presenter, and the wireframe.
         * A wireframe object owns the Activity, Fragment, etc. It is responsible for creating a View/Activity/Fragment and installing it in the window.
         * Since the Presenter contains the logic to react to user inputs, it is the Presenter that knows when to navigate to another screen, and which screen to navigate to.
         * Meanwhile, the wireframe knows how to navigate.
         * So, the Presenter will use the wireframe to perform the navigation.
         * Together, they describe a route from one screen to the next.
         * The wireframe is also an obvious place to handle navigation transition animations.
         */
        public protocol BaseRouting {
            associatedtype ParentView
            associatedtype Presenter
            
            func configure(parent: ParentView)
            func configure(presenter: Presenter)
            func presentViewFromParent()
            func dismissViewFromParent()
        }

        """
     }
     
    public static func createBaseEntity() -> String {
      
        return """
        /**
         * Entity contains basic model objects used by the Interactor.
         *
         * Entities are the model objects manipulated by an Interactor.
         * Entities are only manipulated by the Interactor.
         * The Interactor never passes entities to the presentation layer (i.e. Presenter).
         */
        public protocol BaseEntity: Codable {
        }

        """
     }
    
    public static func createBaseInteractor() -> String {
      
        return """
        /**
         * Interactor contains the business logic as specified by a use case.
         *
         * An Interactor represents a single use case in the app.
         * It contains the business logic to manipulate model objects (Entities) to carry out a specific task.
         * The work done in an Interactor should be independent of any UI.
         */
        public protocol BaseInteractor {
            associatedtype Output
            
            func configure(output: Output)
        }

        public protocol BaseInteractorOutput {}
        public protocol BaseInteractorInput {}


        """
     }
    
    public static func createBasePresenter() -> String {
      
        return """
        /**
         * Presenter contains view logic for preparing content for display (as received from the Interactor) and for reacting to user inputs (by requesting new data from the Interactor).
         *
         * Entities are never passed from the Interactor to the Presenter.
         * Instead, simple data structures that have no behavior are passed from the Interactor to the Presenter.
         * This prevents any ‘real work’ from being done in the Presenter.
         * The Presenter can only prepare the data for display in the View.
         */
        public protocol BasePresenter: ViewAttachable {
            
            associatedtype Interactor
            associatedtype Router
            
            func configure(interactor: Interactor)
            func configure(router: Router)
            
            
        }

        public protocol ViewAttachable {
            associatedtype View
            func attachView(view: View)
            func detachView()
        }


        """
    }
    
    public static func createBaseView() -> String {
     
       return """
       /**
        * View displays what it is told to by the Presenter and relays user input back to the Presenter.
        *
        * The View is passive.
        * It waits for the Presenter to give it content to display;
        * it never asks the Presenter for data.
        * Methods defined for a View should allow a Presenter to communicate at a higher level of abstraction,
        * expressed in terms of its content,
        * and not how that content is to be displayed.
        */
       public protocol BaseView {
           typealias Main = () -> Void
           func run(action: @escaping Main)
       }


       """
    }
}
