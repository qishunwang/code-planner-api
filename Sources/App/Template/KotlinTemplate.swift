 
import Foundation

final class KotlinTemplate {
    //MARK: Module Assembler & AppDependencies
    public static func createAppAssembler(at root: String, package: String, _ modules: [String]) throws {
        typealias Template = KotlinTemplate
        
        let folder = root + "/android-kt/"
        var file = folder + "AppModuleAssembler.kt"
        var data = Template.createAppModuleAssembler(package: package, modules).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
         
        file = folder + "AppDependencies.kt"
        data = Template.createAppDependencies(package: package, modules).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
        file = folder + "RootActivity.kt"
        data = Template.createRoot(package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
        file = folder + "App.kt"
        data = Template.createApp(package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
    }
    private static func createApp(package: String) -> String {
        return """
        package \(package)

        import android.app.Application
        /*
        <manifest xmlns:android="http://schemas.android.com/apk/res/android"
            
            ...

            <application
             
                ...
                
                android:name=".App"
                >
                <activity android:name=".RootActivity">
                    <intent-filter>
                        <action android:name="android.intent.action.MAIN" />

                        <category android:name="android.intent.category.LAUNCHER" />
                    </intent-filter>
                </activity>
            </application>

        </manifest>
        */
        class App: Application(){

            lateinit var appDependencies: AppDependencies

            override fun onCreate() {
                super.onCreate()
                appDependencies = AppDependencies(this)
            }
        }
        """
    }
    
    private static func createRoot(package: String) -> String {
        return """
        package \(package)
        import android.app.Activity
        import android.os.Bundle
        import android.view.ViewGroup
        import androidx.constraintlayout.widget.ConstraintLayout
        import \(package).core.base.RoutingStateManager
        import \(package).core.ui.Utils

        class RootActivity: Activity() {
           private lateinit var root: ViewGroup

           override fun onCreate(savedInstanceState: Bundle?) {
               super.onCreate(savedInstanceState)
               root = ConstraintLayout(this)
               root.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
               setContentView(root)

               (application as? App)?.appDependencies?.installRootView(this, root)

           }

            override fun onAttachedToWindow() {
                super.onAttachedToWindow()
                Utils.updateStatusBarOn(window,true)
            }

           override fun onDestroy() {
               super.onDestroy()
               (application as? App)?.appDependencies?.uninstallRootView()
           }

            override fun onBackPressed() {
                if (RoutingStateManager.Notifier.onBackPressed()) {
                    return super.onBackPressed()
                }
            }
        }

        """
    }
    
    private static func createAppDependencies(package: String, _ modules: [String]) -> String {
        var configures: String = ""
        var presentations: String = ""
        var dismissals: String = ""
//        var setupManagers: String = ""
        var managers: String = ""
        var setupModules: String = ""
        var imports: String = ""
        for (i, module) in modules.enumerated() {
//            imports.append("import \(package).modules.\(module.lowercasedTrimed).logic.manager.\(module.trimed)DataManager;\n")
//            managers.append("\t\t\(module.trimed)DataManager \(module.camelized)DataManager = new \(module.trimed)DataManager();\n")
            configures.append("\t\tassembler.\(module.camelized)Wireframe.parentView = root\n")
            presentations.append("\t\t\(i == 0 ? "":"//")assembler.\(module.camelized)Wireframe.presentViewFromParent()\n")
            dismissals.append("\t\t//assembler.\(module.camelized)Wireframe.dismissViewFromParent()\n")
//            setupManagers.append("\t\t\(module.trimed)DataManager \(module.camelized)DataManager = new \(module.trimed)DataManager();\n")
            setupModules.append("\t\tassembler.setup\(module.trimed)Module()\n")
        }
        return """
        package \(package)

        import android.content.Context
        import android.view.ViewGroup
        
        \(imports)
        class AppDependencies(app: Context) {

            val assembler = AppModuleAssembler()

            init {
        \(setupModules)
                assembler.setupModuleDelegate()
            }

            fun installRootView(activity: Context, root: ViewGroup) {
        \(configures)
        \(presentations)
            }

            fun uninstallRootView(){
        \(dismissals)
            }

        }
        """
    }
    private static func createAppModuleAssembler(package: String, _ modules: [String]) -> String {
        var wireframes: String = ""
        var setupModules: String = ""
        var setupModuleDelegates: String = ""
        var imports: String = ""
        var instances: String = ""
        var getters: String = ""
        for module in modules {
            imports.append("import \(package).modules.\(module.lowercasedTrimed).logic.interactor.\(module.trimed)Interactor\n")
            imports.append("import \(package).modules.\(module.lowercasedTrimed).mi.\(module.trimed)ModuleDelegate\n")
            imports.append("import \(package).modules.\(module.lowercasedTrimed).ui.presenter.\(module.trimed)Presenter\n")
            imports.append("import \(package).modules.\(module.lowercasedTrimed).ui.wireframe.\(module.trimed)Wireframe\n")
            imports.append("import \(package).modules.\(module.lowercasedTrimed).ui.wireframe.\(module.trimed)WireframeInterface\n")
            

            instances.append("\tlateinit var \(module.camelized)Presenter: \(module.trimed)Presenter\n")
            instances.append("\tlateinit var \(module.camelized)Wireframe: \(module.trimed)WireframeInterface\n")
            
            setupModules.append("""

                    // MARK: Setup \(module) Module
                    fun setup\(module.trimed)Module() {
                        val \(module.camelized)Interactor = \(module.trimed)Interactor()
                        \(module.camelized)Wireframe = \(module.trimed)Wireframe()
                        \(module.camelized)Presenter = \(module.trimed)Presenter()

                        \(module.camelized)Wireframe.presenter = \(module.camelized)Presenter
                        \(module.camelized)Presenter.routing = \(module.camelized)Wireframe
                        \(module.camelized)Presenter.interactor = \(module.camelized)Interactor

                        \(module.camelized)Interactor.output = \(module.camelized)Presenter
                    }

                """)
            setupModuleDelegates.append("""

                    // \(module) Module
                    val \(module.camelized)ModuleDelegate = \(module.camelized)Presenter.moduleDelegate as ArrayList<\(module.trimed)ModuleDelegate>
                """)
        }
        return """
        package \(package)
        
        \(imports)
        class AppModuleAssembler {
        \(instances)
        \(getters)
        \(setupModules)
         // MARK: Setup Module Delegate
            fun setupModuleDelegate() {
                \(setupModuleDelegates)
            }
        }

        """
    }
    
    //MARK:Core UI
    public static func createCoreUI(at root: String, package:String) throws {
        typealias Template = KotlinTemplate

        let folder = root + "/android-kt/core/ui/"
        var file = folder + "Utils.kt"
        var data = Template.createUtils(package:package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "DismissalTransition.kt"
        data = Template.createDismissalTransition(package:package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "PresentationTransition.kt"
        data = Template.createPresentationTransition(package:package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

    }
    
    public static func createUtils(package:String) -> String {
        return """
        package \(package).core.ui

        import android.content.res.Resources
        import android.graphics.Color
        import android.os.Build
        import android.view.View
        import android.view.Window
        import android.view.WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS
        import androidx.core.view.WindowCompat

        object Utils {

            fun updateStatusBarOn(window: Window, isDarkness: Boolean) {
                window.statusBarColor = Color.TRANSPARENT

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                    if (isDarkness) {
                        // insetsController is not null when onAttachedToWindow event happened.
                            window.insetsController?.setSystemBarsAppearance(
                                APPEARANCE_LIGHT_STATUS_BARS,
                                APPEARANCE_LIGHT_STATUS_BARS
                            )
                    }
                } else {
                    if (isDarkness) {
                        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                    }
                }

                WindowCompat.setDecorFitsSystemWindows(window,false)
            }

            fun getStatusBarHeight(resources: Resources): Int {
                var result = 0
                val resourceId: Int = resources.getIdentifier("status_bar_height", "dimen", "android")
                if (resourceId > 0) {
                    result = resources.getDimensionPixelSize(resourceId)
                }
                return result
            }
        }

        """
    }
    
    public static func createDismissalTransition(package:String) -> String {
      
        return """
        package \(package).core.ui

        import android.view.ViewGroup
        import android.view.animation.TranslateAnimation

        class DismissalTransition(listener: AnimationListener?, parent: ViewGroup):
            TranslateAnimation(0f, parent.width.toFloat(), 0f, 0f) {
            fun animateTransition(child: ViewGroup) {
                child.clearAnimation()
                child.animation = this
                child.animate()
            }

            init {
                duration = 300
                setAnimationListener(listener)
            }
        }
        """
    }
    public static func createPresentationTransition(package:String) -> String {
     
        return """
        package \(package).core.ui

        import android.view.ViewGroup
        import android.view.animation.TranslateAnimation

        class PresentationTransition(listener: AnimationListener?, parent: ViewGroup):
            TranslateAnimation(parent.width.toFloat(), 0f, 0f, 0f) {
            fun animateTransition(child: ViewGroup) {
                child.clearAnimation()
                child.animation = this
                child.animate()
            }

            init {
                duration = 1000
                setAnimationListener(listener)
            }
        }

       """
    }
    
    //MARK:Core Base
    public static func createCoreBase(at root: String, package:String) throws {
        typealias Template = KotlinTemplate

        let folder = root + "/android-kt/core/base/"
        var file = folder + "BaseModulePresenter.kt"
        var data = Template.createBaseModulePresenter(package:package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "BaseRouting.kt"
        data = Template.createBaseRouting(package:package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "BaseEntity.kt"
        data = Template.createBaseEntity(package:package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)


        file = folder + "BasePresenter.kt"
        data = Template.createBasePresenter(package:package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "BaseInteractor.kt"
        data = Template.createBaseInteractor(package:package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "BaseInteractorInput.kt"
        data = Template.createBaseInteractorInput(package:package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "BaseInteractorOutput.kt"
        data = Template.createBaseInteractorOutput(package:package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "BaseView.kt"
        data = Template.createBaseView(package:package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
       
        file = folder + "RoutingHandler.kt"
        data = Template.createRoutingHandler(package:package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
        file = folder + "RoutingListener.kt"
        data = Template.createRoutingListener(package:package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
        file = folder + "RoutingStateManager.kt"
        data = Template.createRoutingStateManager(package:package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
    }
    public static func createRoutingStateManager(package:String) -> String {
        return """
        package \(package).core.base

        class RoutingStateManager {

            object Notifier {
                // Return false to suppress activity's onBackPressed Event
                fun onBackPressed(): Boolean {

                    val blockTargets = observers.filter { observer -> observer.state == RoutingState.BLOCK }
                    if (blockTargets.isNotEmpty()) {
                        observers.forEach { observer ->
                            observer.willBlockBackPressActionFrom(blockTargets)
                        }
                        return false
                    }

                    val appearTargets =
                        observers.filter { observer -> observer.state == RoutingState.APPEAR }

                    if (appearTargets.isNotEmpty()) {
                        observers.forEach { observer ->
                            observer.willExecuteBackPressed(appearTargets)
                        }

                        var flag = false
                        appearTargets.forEach { target ->
                            if (target.shouldExecuteBackPressed()) {
                                flag = true
                            }
                        }
                        observers.forEach { observer -> observer.didExecuteBackPressed(appearTargets, flag) }
                        return flag
                    } else {
                        return  false
                    }
                }
            }

            companion object Register {
                private var observers = mutableListOf<RoutingStateObserver>()

                fun make(observer: RoutingStateObserver) {
                    if (observers.contains(observer)) return

                    observers.add(observer)
                }

            }
        }


        interface RoutingStateObserver: HardwareNotifyReceivable {
            var state: RoutingState
                get() = RoutingState.DISAPPEAR
                set(_) = TODO()
        }

        enum class RoutingState {
            PAUSING,
            TRANSITION,
            APPEAR,
            DISAPPEAR,
            BLOCK
        }

        interface HardwareNotifyReceivable {
            fun willBlockBackPressActionFrom(blockers: List<RoutingStateObserver>){}
            fun willExecuteBackPressed(executors: List<RoutingStateObserver>){}
            fun shouldExecuteBackPressed(): Boolean{
                return false
            }
            fun didExecuteBackPressed(executors: List<RoutingStateObserver>, isPressed: Boolean){}

        }
        """
    }
    public static func createRoutingListener(package:String) -> String {
      
        return """
        package \(package).core.base

        interface RoutingListener {
            fun didEndDismissRouting(){}
            fun didStartDismissRouting(){}
            fun didStartPresentRouting(){}
            fun didEndPresentRouting(){}
        }
        """
    }
    public static func createRoutingHandler(package:String) -> String {
      
        return """
        package \(package).core.base

        interface RoutingHandler {
            enum class Type {PRESENT, DISMISS}
            fun transition(type: Type)
        }
        """
    }
    
    public static func createBaseModulePresenter(package:String) -> String {
      
        return """
        package \(package).core.base

        interface BaseModulePresenter<M, V: BaseView, I: BaseInteractorInput, R>: BasePresenter<V, I, R> {
            var moduleDelegate: M
        }

        """
    }
     
    public static func createBaseRouting(package: String) -> String {
      
        return """
        package \(package).core.base

        /**
         * Routing contains navigation logic for describing which screens are shown in which order.
         *
         * Routes from one screen to another are defined in the wireframes created by an interaction designer.
         * In VIPER, the responsibility for Routing is shared between two objects: the Presenter, and the wireframe.
         * A wireframe object owns the Activity, Fragment, etc. It is responsible for creating a View/Activity/Fragment and installing it in the window.
         * Since the Presenter contains the logic to react to user inputs, it is the Presenter that knows when to navigate to another screen, and which screen to navigate to.
         * Meanwhile, the wireframe knows how to navigate.
         * So, the Presenter will use the wireframe to perform the navigation.
         * Together, they describe a route from one screen to the next.
         * The wireframe is also an obvious place to handle navigation transition animations.
         */

        interface BaseRouting<ParentView, P> {
            var routingListener: RoutingListener?
            var parentView: ParentView
            var presenter: P

            fun presentViewFromParent(){}
            fun dismissViewFromParent(){}
        }


        """
     }
     
    public static func createBaseEntity(package: String) -> String {
      
        return """
        package \(package).core.base

        /**
         * Entity contains basic model objects used by the Interactor.
         *
         * Entities are the model objects manipulated by an Interactor.
         * Entities are only manipulated by the Interactor.
         * The Interactor never passes entities to the presentation layer (i.e. Presenter).
         */
        interface BaseEntity {
        }
        """
     }
    
    public static func createBaseInteractor(package: String) -> String {
      
        return """
        package \(package).core.base

        /**
         * Interactor contains the business logic as specified by a use case.
         *
         * An Interactor represents a single use case in the app.
         * It contains the business logic to manipulate model objects (Entities) to carry out a specific task.
         * The work done in an Interactor should be independent of any UI.
         */
        interface BaseInteractor<Output> {
            var output: Output

        }
        """
     }
    
    public static func createBaseInteractorInput(package: String) -> String {
        return """
        package \(package).core.base

        interface BaseInteractorInput {
        }

        """
    }
    public static func createBaseInteractorOutput(package: String) -> String {
        return """
        package \(package).core.base

        interface BaseInteractorOutput {
        }
        """
    }
    
    public static func createBasePresenter(package: String) -> String {
      
        return """
        package \(package).core.base

        /**
         * Presenter contains view logic for preparing content for display (as received from the Interactor) and for reacting to user inputs (by requesting new data from the Interactor).
         *
         * Entities are never passed from the Interactor to the Presenter.
         * Instead, simple data structures that have no behavior are passed from the Interactor to the Presenter.
         * This prevents any ‘real work’ from being done in the Presenter.
         * The Presenter can only prepare the data for display in the View.
         */
        interface BasePresenter<V: BaseView,I,R> {

            var view: V?
            var interactor: I
            var routing: R

            fun attachView(view: V) {
                willAttachView(view)
                this.view = view
                this.view?.present()
                didAttachView(view)
            }
            fun detachView(view: V) {
                willDetachView(view)
                this.view?.dismiss()
                didDetachView(view)
                this.view = null
            }

            fun willAttachView(view: V){}
            fun didAttachView(view: V){}
            fun willDetachView(view: V){}
            fun didDetachView(view: V){}
        }
        """
    }
    
    public static func createBaseView(package: String) -> String {
     
       return """
        package \(package).core.base
        import android.os.Looper
        import android.os.Handler
        /**
        * View displays what it is told to by the Presenter and relays user input back to the Presenter.
        *
        * The View is passive.
        * It waits for the Presenter to give it content to display;
        * it never asks the Presenter for data.
        * Methods defined for a View should allow a Presenter to communicate at a higher level of abstraction,
        * expressed in terms of its content,
        * and not how that content is to be displayed.
        */
        interface BaseView {
             var routingHandler: RoutingHandler?

             fun present() {
                 routingHandler?.transition(RoutingHandler.Type.PRESENT)
             }

             fun dismiss() {
                 routingHandler?.transition(RoutingHandler.Type.DISMISS)
             }

             fun run(runnable: Runnable) {
                 Handler(Looper.getMainLooper()).post(runnable)
             }

             fun run(runnable: Runnable, delayed: Long) {
                 Handler(Looper.getMainLooper()).postDelayed(runnable, delayed)
             }
        }

       """
    }
    //MARK:MI
    public static func createModuleDelegate(name:String, package: String) -> String {
        return """
        package \(package).modules.\(name.lowercasedTrimed).mi

        interface \(name.trimed)ModuleDelegate {
            
        }
        """
    }
    
    public static func createModuleInterface(name:String, package: String) -> String {
        return """
        package \(package).modules.\(name.lowercasedTrimed).mi

        interface \(name.trimed)ModuleInterface {
            
        }
        """
    }
    //MARK:UI
    /// View
    public static func createView(name:String, package: String) -> String {
        return """
        package \(package).modules.\(name.lowercasedTrimed).ui.view

        import android.content.Context
        import android.graphics.Color
        import android.widget.TextView
        import androidx.constraintlayout.widget.ConstraintLayout
        import androidx.constraintlayout.widget.ConstraintSet

        import \(package).core.base.RoutingHandler
        import \(package).core.ui.Utils
        import \(package).modules.\(name.lowercasedTrimed).mi.\(name.trimed)ModuleInterface

        class \(name.trimed)View(context: Context):
            ConstraintLayout(context),
            \(name.trimed)ViewInterface {
            var eventHandler: \(name.trimed)ModuleInterface? = null
            override var routingHandler: RoutingHandler? = null

            var textView: TextView

            init {
                id = generateViewId()
                layoutParams = LayoutParams(LayoutParams.MATCH_CONSTRAINT,LayoutParams.MATCH_CONSTRAINT)
                setBackgroundColor(Color.BLUE)
                textView = TextView(context)
                textView.id = generateViewId()
                textView.layoutParams = LayoutParams(LayoutParams.MATCH_CONSTRAINT,LayoutParams.MATCH_CONSTRAINT)
                textView.text = "Hello world"
                addView(textView)
                setupPortraitConstraint()
            }

            override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
                super.onLayout(changed, l, t, r, b)
                if (changed) {
                    val width = r-l
                    val height = b-t

                }
            }

           fun setupPortraitConstraint(){
               var clp: LayoutParams
               clp = (layoutParams as LayoutParams)
               clp.topToTop = ConstraintSet.PARENT_ID
               clp.leftToLeft = ConstraintSet.PARENT_ID
               clp.rightToRight = ConstraintSet.PARENT_ID
               clp.bottomToBottom = ConstraintSet.PARENT_ID
               layoutParams = clp

               clp = (textView.layoutParams as LayoutParams)
               clp.topToTop = ConstraintSet.PARENT_ID
               clp.leftToLeft = ConstraintSet.PARENT_ID
               clp.rightToRight = ConstraintSet.PARENT_ID
               clp.topMargin = Utils.getStatusBarHeight(resources)
               clp.height = 60
               textView.layoutParams = clp

           }

        }
        """
    }
    
    public static func createViewInterface(name:String, package: String) -> String {
        return """
        package \(package).modules.\(name.lowercasedTrimed).ui.view

        import \(package).core.base.BaseView

        interface \(name.trimed)ViewInterface: BaseView {
        }
        """
    }
    /// wireframe
    public static func createWireframeInterface(name:String, package: String) -> String {
        return """
        package \(package).modules.\(name.lowercasedTrimed).ui.wireframe

        import android.view.ViewGroup
        import \(package).core.base.BaseRouting
        import \(package).modules.\(name.lowercasedTrimed).ui.presenter.\(name.trimed)PresenterInterface

        interface \(name.trimed)WireframeInterface: BaseRouting<ViewGroup, \(name.trimed)PresenterInterface> {
        }
        """
    }
    
    public static func createWireframe(name:String, package: String) -> String {
        
        return """
        package \(package).modules.\(name.lowercasedTrimed).ui.wireframe

        import android.util.Log
        import android.view.ViewGroup
        import android.view.animation.Animation
        import android.view.animation.Animation.AnimationListener
        import \(package).core.base.*
        import \(package).core.ui.DismissalTransition
        import \(package).core.ui.PresentationTransition
        import \(package).modules.\(name.lowercasedTrimed).ui.presenter.\(name.trimed)PresenterInterface
        import \(package).modules.\(name.lowercasedTrimed).ui.view.\(name.trimed)View

        class \(name.trimed)Wireframe: \(name.trimed)WireframeInterface, RoutingHandler, RoutingStateObserver {
            override var routingListener: RoutingListener? = null
            override lateinit var presenter: \(name.trimed)PresenterInterface
            override lateinit var parentView: ViewGroup
            override var state: RoutingState = RoutingState.DISAPPEAR
            private lateinit var \(name.camelized)View: \(name.trimed)View
            
            init {
                RoutingStateManager.make(this)
            }

            override fun presentViewFromParent() {
                super.presentViewFromParent()
                if (state != RoutingState.DISAPPEAR){
                    Log.w(javaClass.name,"RoutingState is not at DISAPPEAR state.")
                    return
                }

                \(name.camelized)View = \(name.trimed)View(parentView.context)
                \(name.camelized)View.routingHandler = this
                \(name.camelized)View.eventHandler = presenter

                routingListener = presenter
                parentView.addView(\(name.camelized)View)
                presenter.attachView(\(name.camelized)View)
                state = RoutingState.APPEAR
            }

            override fun dismissViewFromParent() {
                super.dismissViewFromParent()
                presenter.detachView(\(name.camelized)View)
            }

            fun releaseView(){
                (\(name.camelized)View.parent as ViewGroup).removeView(\(name.camelized)View)
                \(name.camelized)View.routingHandler = null
            }

            override fun didExecuteBackPressed(executors: List<RoutingStateObserver>, isPressed: Boolean) {
                executors.forEach { executor ->
        //            if (executor is XXWireframeInterface){
        //                state = RoutingState.APPEAR
        //            }
                }
            }

            override fun transition(type: RoutingHandler.Type) {
                if (state == RoutingState.TRANSITION) {
                    Log.d(javaClass.name,"RoutingState is at TRANSITION state.")
                    return
                }
                when (type) {
                    RoutingHandler.Type.PRESENT -> {
                        PresentationTransition(object: AnimationListener {
                            override fun onAnimationStart(animation: Animation) {
                                routingListener?.didStartPresentRouting()
                                state = RoutingState.TRANSITION
                            }

                            override fun onAnimationEnd(animation: Animation) {
                                routingListener?.didEndPresentRouting()
                                state = RoutingState.APPEAR
                            }

                            override fun onAnimationRepeat(animation: Animation) {}
                        }, parentView).animateTransition(\(name.camelized)View)
                    }

                    RoutingHandler.Type.DISMISS -> {
                        DismissalTransition(object: AnimationListener {
                            override fun onAnimationStart(animation: Animation) {
                                routingListener?.didStartDismissRouting()
                                state = RoutingState.TRANSITION
                            }

                            override fun onAnimationEnd(animation: Animation) {
                                routingListener?.didEndDismissRouting()
                                state = RoutingState.DISAPPEAR
                                releaseView()
                            }

                            override fun onAnimationRepeat(animation: Animation) {}
                        }, parentView).animateTransition(\(name.camelized)View)
                    }
                }
            }

        }



        """
    }
    // presenter
    public static func createPresenterInterface(name:String, package: String) -> String {
        return """
        package \(package).modules.\(name.lowercasedTrimed).ui.presenter

        import \(package).core.base.BaseModulePresenter
        import \(package).core.base.RoutingListener
        import \(package).modules.\(name.lowercasedTrimed).logic.interactor.\(name.trimed)InteractorInterface
        import \(package).modules.\(name.lowercasedTrimed).logic.interactor.\(name.trimed)InteractorOutput
        import \(package).modules.\(name.lowercasedTrimed).mi.\(name.trimed)ModuleDelegate
        import \(package).modules.\(name.lowercasedTrimed).mi.\(name.trimed)ModuleInterface
        import \(package).modules.\(name.lowercasedTrimed).ui.view.\(name.trimed)ViewInterface
        import \(package).modules.\(name.lowercasedTrimed).ui.wireframe.\(name.trimed)WireframeInterface

        interface \(name.trimed)PresenterInterface :
            BaseModulePresenter<List<\(name.trimed)ModuleDelegate>, \(name.trimed)ViewInterface, \(name.trimed)InteractorInterface, \(name.trimed)WireframeInterface>,
            \(name.trimed)InteractorOutput,
            RoutingListener,
            \(name.trimed)ModuleInterface
        {
        }
        """
    }
    
    public static func createPresenter(name:String, package: String) -> String {
        return """
        package \(package).modules.\(name.lowercasedTrimed).ui.presenter

        import \(package).modules.\(name.lowercasedTrimed).logic.interactor.\(name.trimed)InteractorInterface
        import \(package).modules.\(name.lowercasedTrimed).mi.\(name.trimed)ModuleDelegate
        import \(package).modules.\(name.lowercasedTrimed).mi.\(name.trimed)ModuleInterface
        import \(package).modules.\(name.lowercasedTrimed).ui.view.\(name.trimed)ViewInterface
        import \(package).modules.\(name.lowercasedTrimed).ui.wireframe.\(name.trimed)WireframeInterface

        class \(name.trimed)Presenter:
            \(name.trimed)PresenterInterface,
            \(name.trimed)ModuleInterface
        {
            override var moduleDelegate: List<\(name.trimed)ModuleDelegate> = mutableListOf()

            override var view: \(name.trimed)ViewInterface? = null

            override lateinit var interactor: \(name.trimed)InteractorInterface

            override lateinit var routing: \(name.trimed)WireframeInterface
        }
        """
    }
    
    // interactor
    public static func createInteractorInput(name:String, package: String) -> String {
        return """
        package \(package).modules.\(name.lowercasedTrimed).logic.interactor

        import \(package).core.base.BaseInteractorInput

        interface \(name.trimed)InteractorInput: BaseInteractorInput {
        }
        """
    }
    public static func createInteractorOutput(name:String, package: String) -> String {
        return """
        package \(package).modules.\(name.lowercasedTrimed).logic.interactor

        import \(package).core.base.BaseInteractorOutput

        interface \(name.trimed)InteractorOutput: BaseInteractorOutput {
        }
        """
    }

    public static func createInteractor(name:String, package: String) -> String {
        return """
        package \(package).modules.\(name.lowercasedTrimed).logic.interactor

        class \(name.trimed)Interactor: \(name.trimed)InteractorInterface{
            override lateinit var output: \(name.trimed)InteractorOutput
        }
        """
    }
    
    public static func createInteractorInterface(name:String, package: String) -> String {
        return """
        package \(package).modules.\(name.lowercasedTrimed).logic.interactor

        import \(package).core.base.BaseInteractor

        interface \(name.trimed)InteractorInterface: BaseInteractor<\(name.trimed)InteractorOutput>, \(name.trimed)InteractorInput {
        }
        """
    }
    
//    public static func createDataManager(name:String, package: String) -> String {
//        return """
//        package \(package).modules.\(name.lowercasedTrimed).logic.manager;
//
//        //import \(package).models.entities.\(name.lowercasedTrimed).\(name.trimed);
//
//        public class \(name.trimed)DataManager {
//
//        }
//        """
//    }
    
    public static func createKotlinTemplates(at root: String, package: String, module_name: String) throws {
        typealias Template = KotlinTemplate
        var folder = root + "/android-kt/modules/\(module_name.lowercasedTrimed)/mi/"
        var file = folder + "\(module_name.trimed)ModuleDelegate.kt"
        var data = Template.createModuleDelegate(name: module_name, package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "\(module_name.trimed)ModuleInterface.kt"
        data = Template.createModuleInterface(name: module_name, package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)


        folder = root + "/android-kt/modules/\(module_name.lowercasedTrimed)/ui/presenter/"
        file = folder + "\(module_name.trimed)PresenterInterface.kt"
        data = Template.createPresenterInterface(name: module_name, package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "\(module_name.trimed)Presenter.kt"
        data = Template.createPresenter(name: module_name, package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        folder = root + "/android-kt/modules/\(module_name.lowercasedTrimed)/ui/wireframe/"
        file = folder + "\(module_name.trimed)Wireframe.kt"
        data = Template.createWireframe(name: module_name, package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "\(module_name.trimed)WireframeInterface.kt"
        data = Template.createWireframeInterface(name: module_name, package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)


        folder = root + "/android-kt/modules/\(module_name.lowercasedTrimed)/ui/view/"
        file = folder + "\(module_name.trimed)ViewInterface.kt"
        data = Template.createViewInterface(name: module_name, package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "\(module_name.trimed)View.kt"
        data = Template.createView(name: module_name, package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        folder = root + "/android-kt/modules/\(module_name.lowercasedTrimed)/logic/interactor/"
        file = folder + "\(module_name.trimed)InteractorInterface.kt"
        data = Template.createInteractorInterface(name: module_name, package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        folder = root + "/android-kt/modules/\(module_name.lowercasedTrimed)/logic/interactor/"
        file = folder + "\(module_name.trimed)Interactor.kt"
        data = Template.createInteractor(name: module_name, package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "\(module_name.trimed)InteractorInput.kt"
        data = Template.createInteractorInput(name: module_name, package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "\(module_name.trimed)InteractorOutput.kt"
        data = Template.createInteractorOutput(name: module_name, package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)


//        folder = root + "/android-kt/modules/\(module_name.lowercasedTrimed)/logic/manager/"
//        file = folder + "\(module_name.trimed)DataManager.java"
//        data = Template.createDataManager(name: module_name, package: package).data(using: .utf8)
//        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
//        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
    }
}
