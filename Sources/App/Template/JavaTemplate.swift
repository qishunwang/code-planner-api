import Foundation

final class JavaTemplate {
    //MARK: Module Assembler & AppDependencies
    public static func createAppAssembler(at root: String, package: String, _ modules: [String]) throws {
        typealias Template = JavaTemplate
        
        let folder = root + "/android/"
        var file = folder + "AppModuleAssembler.java"
        var data = Template.createAppModuleAssembler(package: package, modules).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
         
        file = folder + "AppDependencies.java"
        data = Template.createAppDependencies(package: package, modules).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
        file = folder + "RootActivity.java"
        data = Template.createRoot(package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
        file = folder + "App.java"
        data = Template.createApp(package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
    }
    private static func createApp(package: String) -> String {
        return """
        package \(package);

        import android.app.Application;
        /*
        <manifest xmlns:android="http://schemas.android.com/apk/res/android"
            
            ...

            <application
             
                ...
                
                android:name=".App"
                >
                <activity android:name=".RootActivity">
                    <intent-filter>
                        <action android:name="android.intent.action.MAIN" />

                        <category android:name="android.intent.category.LAUNCHER" />
                    </intent-filter>
                </activity>
            </application>

        </manifest>
        */
        public class App extends Application {

            private AppDependencies appDependencies;

            @Override
            public void onCreate() {
                super.onCreate();
                appDependencies = new AppDependencies(this);
            }

            public AppDependencies getAppDependencies() {
                return appDependencies;
            }
        }
        """
    }
    
    private static func createRoot(package: String) -> String {
        return """
        package \(package);
        import android.app.Activity;
        import android.os.Bundle;
        import android.view.ViewGroup;

        //import androidx.annotation.NonNull;

        public class RootActivity extends Activity {

            ViewGroup root;

            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                // Create activity_root layout and setup a root view id called root by yourself.
                setContentView(R.layout.activity_root);
                root = findViewById(R.id.root);

            }


            @Override
            protected void onResume() {
                super.onResume();
                ((App)getApplication()).getAppDependencies().installRootView(this, root);
            }

            @Override
            protected void onPause() {
                super.onPause();
                ((App)getApplication()).getAppDependencies().uninstallRootView(root);
            }
        /*
            @Override
            public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                // Create Android Permisstion Manager by your self.
                //((App)getApplication()).getAppDependencies().getAndroidPermission().onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        */
        }

        """
    }
    
    private static func createAppDependencies(package: String, _ modules: [String]) -> String {
        var configures: String = ""
        var presentations: String = ""
        var dismissals: String = ""
        var setupManagers: String = ""
        var managers: String = ""
        var setupModules: String = ""
        var imports: String = ""
        for (i, module) in modules.enumerated() {
            imports.append("import \(package).modules.\(module.lowercasedTrimed).logic.manager.\(module.trimed)DataManager;\n")
            managers.append("\t\t\(module.trimed)DataManager \(module.camelized)DataManager = new \(module.trimed)DataManager();\n")
            configures.append("\t\tassembler.get\(module.trimed)Wireframe().configureParentView(root);\n")
            presentations.append("\t\t\(i == 0 ? "":"//")assembler.get\(module.trimed)Wireframe().present\(module.trimed)ViewFromParent();\n")
            dismissals.append("\t\t//assembler.get\(module.trimed)Wireframe().dismiss\(module.trimed)ViewFromParent();\n")
            setupManagers.append("\t\t\(module.trimed)DataManager \(module.camelized)DataManager = new \(module.trimed)DataManager();\n")
            setupModules.append("\t\tassembler.setup\(module.trimed)Module(\(module.camelized)DataManager);\n")
        }
        return """
        package \(package);

        import android.content.Context;
        import android.view.ViewGroup;
        
        \(imports)
        final class AppDependencies {

            private AppModuleAssembler assembler = new AppModuleAssembler();

            AppDependencies(Context application){
                configure(application);
            }

            private void configure(Context application){
        \(managers)
        \(setupModules)

            }

            //PermissionManager getAndroidPermission(){
            //    return permissionManager;
            //}

            //private void configureAndroidPermission(Context activity){
            //    ((AndroidPermission)permissionManager).configureActivityContext(activity);
            //}

            void installRootView(Context activity, ViewGroup root) {
        \(configures)
        \(presentations)
            }

            void uninstallRootView(ViewGroup root) {
        \(dismissals)
            }

        }

        """
    }
    private static func createAppModuleAssembler(package: String, _ modules: [String]) -> String {
        var wireframes: String = ""
        var setupModules: String = ""
        var imports: String = ""
        var instances: String = ""
        var getters: String = ""
        for module in modules {
            imports.append("import \(package).modules.\(module.lowercasedTrimed).logic.interactor.\(module.trimed)Interactor;\n")
            imports.append("import \(package).modules.\(module.lowercasedTrimed).logic.manager.\(module.trimed)DataManager;\n")
            imports.append("import \(package).modules.\(module.lowercasedTrimed).mi.\(module.trimed)ModuleDelegate;\n")
            imports.append("import \(package).modules.\(module.lowercasedTrimed).ui.presenter.\(module.trimed)Presenter;\n")
            imports.append("import \(package).modules.\(module.lowercasedTrimed).ui.wireframe.\(module.trimed)Wireframe;\n")
            instances.append("\tprivate \(module.trimed)Presenter \(module.camelized)Presenter;\n")
            instances.append("\tprivate \(module.trimed)Wireframe \(module.camelized)Wireframe;\n")
            getters.append("\t\(module.trimed)Wireframe get\(module.trimed)Wireframe() throws NullPointerException {\n\t\treturn \(module.camelized)Wireframe;\n\t}\n\n")
            wireframes.append("\tlet \(module.camelized)Wireframe = \(module.trimed)Wireframe()\n")
            setupModules.append("""
                    // MARK: Setup \(module) Module
                    void setup\(module.trimed)Module(\(module.trimed)DataManager \(module.camelized)DataManager) {
                        
                        \(module.trimed)Interactor \(module.camelized)Interactor = new \(module.trimed)Interactor(\(module.camelized)DataManager);
                        \(module.camelized)Presenter = new \(module.trimed)Presenter();
                        \(module.camelized)Wireframe = new \(module.trimed)Wireframe();

                        \(module.camelized)Presenter.setInteractor(\(module.camelized)Interactor);
                        \(module.camelized)Presenter.setRouting(\(module.camelized)Wireframe);

                        \(module.camelized)Interactor.setInteractorOutput(\(module.camelized)Presenter);
                        \(module.camelized)Wireframe.setPresenter(\(module.camelized)Presenter);
                    }
                
                """)
        }
        return """
        package \(package);
        
        \(imports)
        final class AppModuleAssembler {
        \(instances)
        \(getters)
            \(setupModules)
        }

        """
    }
    
    //MARK:Core UI
    public static func createCoreUI(at root: String, package:String) throws {
        typealias Template = JavaTemplate

        let folder = root + "/android/core/ui/"
        var file = folder + "ViewTransitionDelegate.java"
        var data = Template.createViewTransitionDelegate(package:package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "DismissalTransition.java"
        data = Template.createDismissalTransition(package:package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "PresentationTransition.java"
        data = Template.createPresentationTransition(package:package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

    }
    
    public static func createViewTransitionDelegate(package:String) -> String {
        return """
        package \(package).core.ui;

        public interface ViewTransitionDelegate {
            void shouldAttachToSuperview();
            void shouldRemoveFromSuperview();
        }

        """
    }
    public static func createDismissalTransition(package:String) -> String {
      
        return """
        package \(package).core.ui;

        import android.view.ViewGroup;
        import android.view.animation.TranslateAnimation;

        public class DismissalTransition extends TranslateAnimation {

            public DismissalTransition(AnimationListener listener, ViewGroup parent) {
                super(0,parent.getMeasuredWidth(),0,0);
                setDuration(300);
                setAnimationListener(listener);
            }

            public void animateTransition(ViewGroup child){
                child.setAnimation(this);
                child.animate();
            }
        }
        """
    }
    public static func createPresentationTransition(package:String) -> String {
     
        return """
        package \(package).core.ui;

        import android.view.ViewGroup;
        import android.view.animation.TranslateAnimation;

        public class PresentationTransition extends TranslateAnimation {

            public PresentationTransition(AnimationListener listener, ViewGroup parent) {
                super(parent.getMeasuredWidth(),0,0,0);
                setDuration(400);
                setAnimationListener(listener);
            }

            public void animateTransition(ViewGroup child){
                child.setAnimation(this);
                child.animate();
            }
        }

       """
    }
    
    //MARK:Core Base
    public static func createCoreBase(at root: String, package:String) throws {
        typealias Template = JavaTemplate

        let folder = root + "/android/core/base/"
        var file = folder + "BaseModulePresenter.java"
        var data = Template.createBaseModulePresenter(package:package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "BaseRouting.java"
        data = Template.createBaseRouting(package:package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "BaseEntity.java"
        data = Template.createBaseEntity(package:package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)


        file = folder + "BasePresenter.java"
        data = Template.createBasePresenter(package:package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "BaseInteractor.java"
        data = Template.createBaseInteractor(package:package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "BaseInteractorInput.java"
        data = Template.createBaseInteractorInput(package:package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "BaseInteractorOutput.java"
        data = Template.createBaseInteractorOutput(package:package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "BaseView.java"
        data = Template.createBaseView(package:package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
       
    }
    
    public static func createBaseModulePresenter(package:String) -> String {
      
        return """
        package \(package).core.base;

        public interface BaseModulePresenter<M ,V extends BaseView, I extends BaseInteractorInput, R extends BaseRouting> extends BasePresenter<V,I,R> {

            M getModuleDelegate();

            void setModuleDelegate(M moduleDelegate);
        }

        """
     }
     
    public static func createBaseRouting(package: String) -> String {
      
        return """
        package \(package).core.base;

        /**
         * Routing contains navigation logic for describing which screens are shown in which order.
         *
         * Routes from one screen to another are defined in the wireframes created by an interaction designer.
         * In VIPER, the responsibility for Routing is shared between two objects: the Presenter, and the wireframe.
         * A wireframe object owns the Activity, Fragment, etc. It is responsible for creating a View/Activity/Fragment and installing it in the window.
         * Since the Presenter contains the logic to react to user inputs, it is the Presenter that knows when to navigate to another screen, and which screen to navigate to.
         * Meanwhile, the wireframe knows how to navigate.
         * So, the Presenter will use the wireframe to perform the navigation.
         * Together, they describe a route from one screen to the next.
         * The wireframe is also an obvious place to handle navigation transition animations.
         */
        public interface BaseRouting {
        }

        """
     }
     
    public static func createBaseEntity(package: String) -> String {
      
        return """
        package \(package).core.base;

        /**
         * Entity contains basic model objects used by the Interactor.
         *
         * Entities are the model objects manipulated by an Interactor.
         * Entities are only manipulated by the Interactor.
         * The Interactor never passes entities to the presentation layer (i.e. Presenter).
         */
        public interface BaseEntity {
        }


        """
     }
    
    public static func createBaseInteractor(package: String) -> String {
      
        return """
        package \(package).core.base;

        /**
         * Interactor contains the business logic as specified by a use case.
         *
         * An Interactor represents a single use case in the app.
         * It contains the business logic to manipulate model objects (Entities) to carry out a specific task.
         * The work done in an Interactor should be independent of any UI.
         */
        public interface BaseInteractor<Output> {
            void setInteractorOutput(Output output);
            Output getInteractorOutput() throws NullPointerException;
        }
        """
     }
    
    public static func createBaseInteractorInput(package: String) -> String {
        return """
        package \(package).core.base;

        public interface BaseInteractorInput {
        }

        """
    }
    public static func createBaseInteractorOutput(package: String) -> String {
        return """
        package \(package).core.base;

        public interface BaseInteractorOutput {
        }

        """
    }
    
    public static func createBasePresenter(package: String) -> String {
      
        return """
        package \(package).core.base;

        /**
         * Presenter contains view logic for preparing content for display (as received from the Interactor) and for reacting to user inputs (by requesting new data from the Interactor).
         *
         * Entities are never passed from the Interactor to the Presenter.
         * Instead, simple data structures that have no behavior are passed from the Interactor to the Presenter.
         * This prevents any ‘real work’ from being done in the Presenter.
         * The Presenter can only prepare the data for display in the View.
         */
        public interface BasePresenter<V extends BaseView,  I extends BaseInteractorInput, R extends BaseRouting>  {

            I getInteractor();
            R getRouting();
            V getView();

            void setRouting(R routing);
            void setInteractor(I interactor);

            void attachView(V view);
            void detachView();


        }
        """
    }
    
    public static func createBaseView(package: String) -> String {
     
       return """
       package \(package).core.base;

       /**
        * View displays what it is told to by the Presenter and relays user input back to the Presenter.
        *
        * The View is passive.
        * It waits for the Presenter to give it content to display;
        * it never asks the Presenter for data.
        * Methods defined for a View should allow a Presenter to communicate at a higher level of abstraction,
        * expressed in terms of its content,
        * and not how that content is to be displayed.
        */
       public interface BaseView {
           void run(Runnable runnable);
       }

       """
    }
    //MARK:MI
    public static func createModuleDelegate(name:String, package: String) -> String {
        return """
        package \(package).modules.\(name.lowercasedTrimed).mi;

        public interface \(name.trimed)ModuleDelegate {
            
        }
        """
    }
    
    public static func createModuleInterface(name:String, package: String) -> String {
        return """
        package \(package).modules.\(name.lowercasedTrimed).mi;

        public interface \(name.trimed)ModuleInterface {
            
        }

        """
    }
    //MARK:UI
    /// View
    public static func createView(name:String, package: String) -> String {
        return """
        package \(package).modules.\(name.lowercasedTrimed).ui.view;

        import android.content.Context;
        import android.graphics.Color;
        import android.os.Handler;
        import android.util.Log;
        import android.widget.LinearLayout;
        import android.widget.TextView;

        import \(package).core.ui.ViewTransitionDelegate;
        import \(package).modules.\(name.lowercasedTrimed).mi.\(name.trimed)ModuleInterface;

        public class \(name.trimed)View extends LinearLayout implements \(name.trimed)ViewInterface {

            private \(name.trimed)ModuleInterface eventHandler;
            private ViewTransitionDelegate viewTransitionDelegate;
            
            public \(name.trimed)View(Context context) {
                super(context);
                setBackgroundColor(Color.WHITE);
                setOrientation(LinearLayout.VERTICAL);

                TextView hello_world = new TextView(context);
                hello_world.setBackgroundColor(Color.BLACK);
                hello_world.setTextColor(Color.parseColor("#ff0000"));
                hello_world.setText("Hello World");
                addView(hello_world);
            }
        
            public void setEventHandler(\(name.trimed)ModuleInterface eventHandler) {
                this.eventHandler = eventHandler;
            }

            public \(name.trimed)ModuleInterface getEventHandler() throws NullPointerException {
                return eventHandler;
            }


            public ViewTransitionDelegate getViewTransitionDelegate() {
                return viewTransitionDelegate;
            }

            public void setViewTransitionDelegate(ViewTransitionDelegate viewTransitionDelegate) {
                this.viewTransitionDelegate = viewTransitionDelegate;
            }

            
            @Override
            protected void finalize() throws Throwable {
                super.finalize();
                Log.d("finalize-->",getClass().toString());
            }


            @Override
            public void present() {
                getViewTransitionDelegate().shouldAttachToSuperview();
            }

            @Override
            public void dismiss() {
                getViewTransitionDelegate().shouldRemoveFromSuperview();
            }


            @Override
            public void run(Runnable runnable) {
                Handler mainHandler = new Handler(getContext().getMainLooper());
                mainHandler.post(runnable);
            }

        }

        """
    }
    
    public static func createViewInterface(name:String, package: String) -> String {
        return """
        package \(package).modules.\(name.lowercasedTrimed).ui.view;

        import \(package).core.base.BaseView;

        public interface \(name.trimed)ViewInterface extends BaseView {
            void present();
            void dismiss();
        }

        """
    }
    /// wireframe
    public static func createWireframeInterface(name:String, package: String) -> String {
        return """
        package \(package).modules.\(name.lowercasedTrimed).ui.wireframe;

        import \(package).core.base.BaseRouting;

        public interface \(name.trimed)WireframeInterface extends BaseRouting {
            void present\(name.trimed)ViewFromParent();
            void dismiss\(name.trimed)ViewFromParent();
        }
        """
    }
    
    public static func createWireframe(name:String, package: String) -> String {
        
        return """
        package \(package).modules.\(name.lowercasedTrimed).ui.wireframe;

        import android.util.Log;
        import android.view.ViewGroup;
        import android.view.animation.Animation;

        import \(package).core.ui.PresentationTransition;
        import \(package).core.ui.DismissalTransition;
        import \(package).core.ui.ViewTransitionDelegate;
        //import \(package).modules.\(name.lowercasedTrimed).ui.transition.\(name.trimed)DismissalTransition;
        //import \(package).modules.\(name.lowercasedTrimed).ui.transition.\(name.trimed)PresentationTransition;
        import \(package).modules.\(name.lowercasedTrimed).ui.presenter.\(name.trimed)Presenter;
        import \(package).modules.\(name.lowercasedTrimed).ui.view.\(name.trimed)View;
        import \(package).modules.\(name.lowercasedTrimed).ui.wireframe.\(name.trimed)WireframeInterface;
        
        public class \(name.trimed)Wireframe implements \(name.trimed)WireframeInterface, ViewTransitionDelegate {

            private \(name.trimed)Presenter presenter;
            private \(name.trimed)View \(name.camelized)View;
            private ViewGroup parent;

            public void configureParentView(ViewGroup parent) {
                this.parent = parent;
            }

            public void setPresenter(\(name.trimed)Presenter presenter) {
                this.presenter = presenter;
            }

            private void present\(name.trimed)InterfaceFromViewGroup() {
                if (\(name.camelized)View != null) {return;}

                \(name.camelized)View = new \(name.trimed)View(parent.getContext());
                \(name.camelized)View.setEventHandler(presenter);
                \(name.camelized)View.setViewTransitionDelegate(this);

                parent.addView(\(name.camelized)View);
                presenter.attachView(\(name.camelized)View);
            }

            private void dismiss\(name.trimed)InterfaceFromViewGroup(){
                if (\(name.camelized)View == null) return;
                presenter.detachView();
                ((ViewGroup)\(name.camelized)View.getParent()).removeView(\(name.camelized)View);
                \(name.camelized)View = null;
            }


            @Override
            public void shouldAttachToSuperview() {

                new PresentationTransition(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                }, parent).animateTransition(\(name.camelized)View);

            }

            @Override
            public void shouldRemoveFromSuperview() {

                \(name.camelized)View.clearAnimation();

                new DismissalTransition(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        Log.d("onAnimationStart","<======");

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        dismiss\(name.trimed)InterfaceFromViewGroup();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }

                },parent).animateTransition(\(name.camelized)View);



            }

            @Override
            public void present\(name.trimed)ViewFromParent() {
                present\(name.trimed)InterfaceFromViewGroup();
            }

            @Override
            public void dismiss\(name.trimed)ViewFromParent() {
                dismiss\(name.trimed)InterfaceFromViewGroup();
            }
        }

        """
    }
    // presenter
    public static func createPresenterInterface(name:String, package: String) -> String {
        return """
        package \(package).modules.\(name.lowercasedTrimed).ui.presenter;

        import \(package).core.base.BaseModulePresenter;
        import \(package).modules.\(name.lowercasedTrimed).logic.interactor.\(name.trimed)InteractorInput;
        import \(package).modules.\(name.lowercasedTrimed).mi.\(name.trimed)ModuleDelegate;
        import \(package).modules.\(name.lowercasedTrimed).ui.view.\(name.trimed)ViewInterface;
        import \(package).modules.\(name.lowercasedTrimed).ui.wireframe.\(name.trimed)WireframeInterface;

        public interface \(name.trimed)PresenterInterface extends BaseModulePresenter<\(name.trimed)ModuleDelegate, \(name.trimed)ViewInterface, \(name.trimed)InteractorInput, \(name.trimed)WireframeInterface> {}
        """
    }
    
    public static func createPresenter(name:String, package: String) -> String {
        return """
        package \(package).modules.\(name.lowercasedTrimed).ui.presenter;

        import \(package).modules.\(name.lowercasedTrimed).logic.interactor.\(name.trimed)InteractorInput;
        import \(package).modules.\(name.lowercasedTrimed).logic.interactor.\(name.trimed)InteractorOutput;
        import \(package).modules.\(name.lowercasedTrimed).mi.\(name.trimed)ModuleDelegate;
        import \(package).modules.\(name.lowercasedTrimed).mi.\(name.trimed)ModuleInterface;
        import \(package).modules.\(name.lowercasedTrimed).ui.view.\(name.trimed)ViewInterface;
        import \(package).modules.\(name.lowercasedTrimed).ui.wireframe.\(name.trimed)WireframeInterface;

        public class \(name.trimed)Presenter implements \(name.trimed)PresenterInterface, \(name.trimed)InteractorOutput, \(name.trimed)ModuleInterface {

            private \(name.trimed)ViewInterface \(name.camelized)ViewInterface;
            private \(name.trimed)InteractorInput \(name.camelized)InteractorInput;
            private \(name.trimed)WireframeInterface \(name.camelized)Wireframe;
            private \(name.trimed)ModuleDelegate \(name.camelized)ModuleDelegate;

            @Override
            public \(name.trimed)InteractorInput getInteractor() {
                return \(name.camelized)InteractorInput;
            }

            @Override
            public \(name.trimed)WireframeInterface getRouting() {
                return \(name.camelized)Wireframe;
            }

            @Override
            public \(name.trimed)ViewInterface getView() {
                return \(name.camelized)ViewInterface;
            }

            @Override
            public void setRouting(\(name.trimed)WireframeInterface routing) {
                \(name.camelized)Wireframe = routing;
            }

            @Override
            public void setInteractor(\(name.trimed)InteractorInput interactor) {
                this.\(name.camelized)InteractorInput = interactor;
            }

            @Override
            public void attachView(\(name.trimed)ViewInterface view) {
                this.\(name.camelized)ViewInterface = view;
                getView().present();
            }

            @Override
            public void detachView() {
                this.\(name.camelized)ViewInterface = null;
            }

            //BaseModulePresenter methods.
            @Override
            public \(name.trimed)ModuleDelegate getModuleDelegate() {
                return \(name.camelized)ModuleDelegate;
            }

            @Override
            public void setModuleDelegate(\(name.trimed)ModuleDelegate moduleDelegate) {
                \(name.camelized)ModuleDelegate = moduleDelegate;
            }
        }

        """
    }
    // interactor
    public static func createInteractorInput(name:String, package: String) -> String {
        return """
        package \(package).modules.\(name.lowercasedTrimed).logic.interactor;

        import \(package).core.base.BaseInteractorInput;

        public interface \(name.trimed)InteractorInput extends BaseInteractorInput {
            
        }

        """
    }
    public static func createInteractorOutput(name:String, package: String) -> String {
        return """
        package \(package).modules.\(name.lowercasedTrimed).logic.interactor;

        import \(package).core.base.BaseInteractorOutput;

        public interface \(name.trimed)InteractorOutput extends BaseInteractorOutput {
        }
        """
    }

    public static func createInteractor(name:String, package: String) -> String {
        return """
        package \(package).modules.\(name.lowercasedTrimed).logic.interactor;

        //import \(package).models.entities.\(name.lowercasedTrimed).\(name.trimed);
        import \(package).modules.\(name.lowercasedTrimed).logic.manager.\(name.trimed)DataManager;


        public class \(name.trimed)Interactor implements \(name.trimed)InteractorInterface , \(name.trimed)InteractorInput {

            private \(name.trimed)InteractorOutput \(name.camelized)InteractorOutput;

            private \(name.trimed)DataManager \(name.camelized)DataManager;

            public \(name.trimed)Interactor(\(name.trimed)DataManager \(name.camelized)DataManager){
                this.\(name.camelized)DataManager = \(name.camelized)DataManager;
            }

            @Override
            public void setInteractorOutput(\(name.trimed)InteractorOutput output) {
                this.\(name.camelized)InteractorOutput = output;
            }

            @Override
            public \(name.trimed)InteractorOutput getInteractorOutput() throws NullPointerException {
                if (\(name.camelized)InteractorOutput != null) return \(name.camelized)InteractorOutput;
                throw new NullPointerException("You must setup \(name.trimed)InteractorOutput before you use it.");
            }
        }
        """
    }
    
    public static func createInteractorInterface(name:String, package: String) -> String {
        return """
        package \(package).modules.\(name.lowercasedTrimed).logic.interactor;

        import \(package).core.base.BaseInteractor;

        public interface \(name.trimed)InteractorInterface extends BaseInteractor<\(name.trimed)InteractorOutput> {
        }

        """
    }
    
    public static func createDataManager(name:String, package: String) -> String {
        return """
        package \(package).modules.\(name.lowercasedTrimed).logic.manager;

        //import \(package).models.entities.\(name.lowercasedTrimed).\(name.trimed);

        public class \(name.trimed)DataManager {

        }
        """
    }
    
    public static func createJavaTemplates(at root: String, package: String, module_name: String) throws {
        typealias Template = JavaTemplate
        var folder = root + "/android/modules/\(module_name.lowercasedTrimed)/mi/"
        var file = folder + "\(module_name.trimed)ModuleDelegate.java"
        var data = Template.createModuleDelegate(name: module_name, package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "\(module_name.trimed)ModuleInterface.java"
        data = Template.createModuleInterface(name: module_name, package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)


        folder = root + "/android/modules/\(module_name.lowercasedTrimed)/ui/presenter/"
        file = folder + "\(module_name.trimed)PresenterInterface.java"
        data = Template.createPresenterInterface(name: module_name, package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "\(module_name.trimed)Presenter.java"
        data = Template.createPresenter(name: module_name, package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        folder = root + "/android/modules/\(module_name.lowercasedTrimed)/ui/wireframe/"
        file = folder + "\(module_name.trimed)Wireframe.java"
        data = Template.createWireframe(name: module_name, package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "\(module_name.trimed)WireframeInterface.java"
        data = Template.createWireframeInterface(name: module_name, package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)


        folder = root + "/android/modules/\(module_name.lowercasedTrimed)/ui/view/"
        file = folder + "\(module_name.trimed)ViewInterface.java"
        data = Template.createViewInterface(name: module_name, package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "\(module_name.trimed)View.java"
        data = Template.createView(name: module_name, package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        folder = root + "/android/modules/\(module_name.lowercasedTrimed)/logic/interactor/"
        file = folder + "\(module_name.trimed)InteractorInterface.java"
        data = Template.createInteractorInterface(name: module_name, package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        folder = root + "/android/modules/\(module_name.lowercasedTrimed)/logic/interactor/"
        file = folder + "\(module_name.trimed)Interactor.java"
        data = Template.createInteractor(name: module_name, package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "\(module_name.trimed)InteractorInput.java"
        data = Template.createInteractorInput(name: module_name, package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)

        file = folder + "\(module_name.trimed)InteractorOutput.java"
        data = Template.createInteractorOutput(name: module_name, package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)


        folder = root + "/android/modules/\(module_name.lowercasedTrimed)/logic/manager/"
        file = folder + "\(module_name.trimed)DataManager.java"
        data = Template.createDataManager(name: module_name, package: package).data(using: .utf8)
        try FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true)
        let _ = FileManager.default.createFile(atPath: file, contents: data)
        
    }
}


extension String {
    var trimed: String {
        return replacingOccurrences(of: " ", with: "")
    }
    
    var lowercasedTrimed: String {
        return replacingOccurrences(of: " ", with: "_").lowercased()
    }
    var camelized: String {
        let parts = components(separatedBy: .whitespaces)
        var result = ""
        for (i, part) in parts.enumerated() {
            if i == 0 {
                result.append(part.lowercased())
            } else {
                result.append(part)
            }
        }
        
        return result
    }
      
}
