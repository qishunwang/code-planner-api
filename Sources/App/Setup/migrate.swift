import FluentPostgreSQL

/// Register your model's migrations here.
public func migrate(config: inout MigrationConfig) {
    config.add(model: Admin.self, database: .psql)
    config.add(model: Project.self, database: .psql)
    config.add(model: Module.self, database: .psql)
    config.add(model: ProjectModule.self, database: .psql)
    config.add(model: AdminProject.self, database: .psql)
    config.add(model: AdminModule.self, database: .psql)
    config.add(model: AdminGenerateRecord.self, database: .psql)
    
    config.add(migration: AdminMigration.self, database: .psql)
    
    config.prepareCache(for: .psql)
}
