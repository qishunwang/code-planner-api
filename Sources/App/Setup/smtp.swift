import VaporExt

public func setupSMTP(_ services: inout Services) throws {
    let hostname = Environment.get(AppEnvironment.SMTP_HOSTNAME.value) ?? ""
    let username = Environment.get(AppEnvironment.SMTP_USERNAME.value) ?? ""
    let password = Environment.get(AppEnvironment.SMTP_PASSWORD.value) ?? ""
    
    try services.register(SMTPProvider(config: SMTPConfig(hostname: hostname, email: username, password: password)))
}
