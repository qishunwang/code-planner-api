import VaporExt
/// Register your application's routes here.
public func routes(_ router: Router) throws {
    // Basic "It works" example
    router.get { _ in
        return "It works!"
    } 
    let v1 = router.grouped("v1")
  
    try v1.register(collection: AdminController())
    try v1.register(collection: ProjectController())
    try v1.register(collection: ModuleController())
    try v1.register(collection: TemplateController())
    try v1.register(collection: HealthController())
}
