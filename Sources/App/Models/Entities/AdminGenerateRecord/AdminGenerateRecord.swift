import FluentPostgreSQL
import Vapor
 
final class AdminGenerateRecord: Codable {
    var id: UUID?
    var admin_id: Admin.ID
    var location: String
    var info: String
    
    var createdAt: Date?
    var updatedAt: Date?
    var deletedAt: Date?
    
    /// Creates a new `Module`.
    init(id: UUID? = nil
        ,location: String
        ,info: String = ""
        ,admin_id: Admin.ID) {
        self.id = id
        self.location = location
        self.admin_id = admin_id
        self.info = info
    }
}


extension AdminGenerateRecord: PostgreSQLUUIDModel {
    static var entity = "admin_generate_records"
    
    static var createdAtKey: TimestampKey? = \.createdAt
    static var deletedAtKey: TimestampKey? = \.deletedAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}


extension AdminGenerateRecord: Migration {
    typealias Database = PostgreSQLDatabase
}


extension AdminGenerateRecord: Content {}


extension AdminGenerateRecord: Parameter {}


extension AdminGenerateRecord {
    var admin: Parent<AdminGenerateRecord, Admin> {
        return parent(\.admin_id)
    }
}
extension Admin {
    var generateRecords: Children<Admin, AdminGenerateRecord> {
        return children(\.admin_id)
    }
}
