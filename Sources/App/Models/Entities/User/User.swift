import Foundation
import FluentPostgreSQL
import Vapor
import Authentication
/// A single entry of a User list.
final class User: Codable {
    /// The unique identifier for this `User`.
    var id: Int?
    
    /// A title describing what this `User` entails.
    var username: String
    var password: String
    var verifyToken: String
    var createdAt: Date?
    var updatedAt: Date?
    var deletedAt: Date?
    
    /// Creates a new `User`.
    init(id: Int? = nil
        ,username: String
        ,password: String
        ,verifyToken: String
        ) {
        self.id = id
        self.username = username
        self.password = password
        self.verifyToken = verifyToken
    }
}

/// Allows `User` to be used as Fluent model.
extension User: PostgreSQLModel {
    static var entity = "users"
    
    static var createdAtKey: TimestampKey? = \.createdAt
    static var deletedAtKey: TimestampKey? = \.deletedAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}

/// Allows `User` to be used as a dynamic migration.
extension User: Migration {
    typealias Database = PostgreSQLDatabase
}

/// Allows `User` to be encoded to and decoded from HTTP messages.
extension User: Content {}

/// Allows `User` to be used as a dynamic parameter in route definitions.
extension User: Parameter {}

extension User: Validatable {
    static func validations() throws -> Validations<User> {
        var validations = Validations(User.self)
        try validations.add(\.username, !.empty)
        try validations.add(\.password, !.empty)
        return validations
    }
}
extension User: SessionAuthenticatable {}

extension User: PasswordAuthenticatable {
    public static var usernameKey: WritableKeyPath<User, String> {
        return \User.username
    }
    public static var passwordKey: WritableKeyPath<User, String> {
        return \User.password
    }
}

extension User {
    struct `Public`: Content {
        let id: User.ID?
        let username: String
        var createdAt: Date?
        var updatedAt: Date?
    }
    func `public`() -> Public {
        return User.Public(id: id, username: username, createdAt: createdAt, updatedAt: updatedAt)
    }
}
