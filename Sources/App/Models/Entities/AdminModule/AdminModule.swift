import FluentPostgreSQL
import Vapor
 
final class AdminModule: Codable {
     
    var id: Int?
     
    var admin_id: Admin.ID
    var module_id: Module.ID
    var createdAt: Date?
    var updatedAt: Date?
    var deletedAt: Date?
     
    init(id: Int? = nil
        ,admin_id: Admin.ID
        ,module_id: Module.ID
        ) {
        self.id = id
        self.admin_id = admin_id
        self.module_id = module_id
    }
    
    init(_ left: Admin, _ right: Module) throws {
        self.admin_id = try left.requireID()
        self.module_id = try right.requireID()
    }
}
 
extension AdminModule: PostgreSQLPivot {
    static var entity = "admin_module"
    typealias Left = Admin
    typealias Right = Module
    static var leftIDKey: LeftIDKey = \.admin_id
    static var rightIDKey: RightIDKey = \.module_id
    static var createdAtKey: TimestampKey? = \.createdAt
    static var deletedAtKey: TimestampKey? = \.deletedAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}
 
extension AdminModule: Migration {
    typealias Database = PostgreSQLDatabase
}
 
extension AdminModule: Content {}
 
extension AdminModule: Parameter {}

extension AdminModule: ModifiablePivot {}

extension Admin {
    var modules: Siblings<Admin, Module, AdminModule> {
        return siblings()
    }
}

extension Module {
    var admins: Siblings<Module, Admin, AdminModule> {
        return siblings()
    }
}
