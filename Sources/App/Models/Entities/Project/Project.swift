import FluentPostgreSQL
import Vapor
 
final class Project: Codable {
    var id: Int?
    
    var name: String
    var packageName: String
    var isShared: Bool
    
    var createdAt: Date?
    var updatedAt: Date?
    var deletedAt: Date?
    
    init(id: Int? = nil
        ,name: String
        ,packageName: String
        ,isShared: Bool = false
        ) {
        self.id = id
        self.name = name
        self.packageName = packageName
        self.isShared = isShared
    }
}

extension Project: PostgreSQLModel {
    static var entity = "projects"
    
    static var createdAtKey: TimestampKey? = \.createdAt
    static var deletedAtKey: TimestampKey? = \.deletedAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}

extension Project: Migration {
    typealias Database = PostgreSQLDatabase
}

extension Project: Content {}

extension Project: Parameter {}

extension Project: Validatable {
    static func validations() throws -> Validations<Project> {
        var validations = Validations(Project.self)
        try validations.add(\.name, !.empty)
        return validations
    }
}
  
