import FluentPostgreSQL
import Vapor
import Authentication
/// A single entry of a Admin list.
final class Admin: Codable {
    /// The unique identifier for this `Admin`.
    var id: Int?
    
    /// A title describing what this `Admin` entails.
    var username: String
    var password: String
    
    var createdAt: Date?
    var updatedAt: Date?
    var deletedAt: Date?
    
    /// Creates a new `Admin`.
    init(id: Int? = nil
        ,username: String
        ,password: String
        ) {
        self.id = id
        self.username = username
        self.password = password
    }
}

/// Allows `Admin` to be used as Fluent model.
extension Admin: PostgreSQLModel {
    static var entity = "admins"
    
    static var createdAtKey: TimestampKey? = \.createdAt
    static var deletedAtKey: TimestampKey? = \.deletedAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}

/// Allows `Admin` to be used as a dynamic migration.
extension Admin: Migration {
    typealias Database = PostgreSQLDatabase
}

/// Allows `Admin` to be encoded to and decoded from HTTP messages.
extension Admin: Content {}

/// Allows `Admin` to be used as a dynamic parameter in route definitions.
extension Admin: Parameter {}

extension Admin: Validatable {
    static func validations() throws -> Validations<Admin> {
        var validations = Validations(Admin.self)
        try validations.add(\.username, .email)
        try validations.add(\.password, !.empty)
        return validations
    }
}
extension Admin: SessionAuthenticatable {}

extension Admin: PasswordAuthenticatable {
    public static var usernameKey: WritableKeyPath<Admin, String> {
        return \Admin.username
    }
    public static var passwordKey: WritableKeyPath<Admin, String> {
        return \Admin.password
    }
}

extension Admin {
    struct `Public`: Content {
        let id: Admin.ID?
        let username: String
        var createdAt: Date?
        var updatedAt: Date?
    }
    func `public`() -> Public {
        return Admin.Public(id: id, username: username, createdAt: createdAt, updatedAt: updatedAt)
    }
}
