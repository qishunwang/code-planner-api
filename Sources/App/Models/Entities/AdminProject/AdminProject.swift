import FluentPostgreSQL
import Vapor
 
final class AdminProject: Codable {
     
    var id: Int?
     
    var admin_id: Admin.ID
    var project_id: Project.ID
    var createdAt: Date?
    var updatedAt: Date?
    var deletedAt: Date?
     
    init(id: Int? = nil
        ,admin_id: Admin.ID
        ,project_id: Project.ID
        ) {
        self.id = id
        self.admin_id = admin_id
        self.project_id = project_id
    }
    
    init(_ left: Admin, _ right: Project) throws {
        self.admin_id = try left.requireID()
        self.project_id = try right.requireID()
    }
}
 
extension AdminProject: PostgreSQLPivot {
    static var entity = "admin_project"
    typealias Left = Admin
    typealias Right = Project
    static var leftIDKey: LeftIDKey = \.admin_id
    static var rightIDKey: RightIDKey = \.project_id
    static var createdAtKey: TimestampKey? = \.createdAt
    static var deletedAtKey: TimestampKey? = \.deletedAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}
 
extension AdminProject: Migration {
    typealias Database = PostgreSQLDatabase
}
 
extension AdminProject: Content {}
 
extension AdminProject: Parameter {}

extension AdminProject: ModifiablePivot {}

extension Admin {
    var projects: Siblings<Admin, Project, AdminProject> {
        return siblings()
    }
}

extension Project {
    var admins: Siblings<Project, Admin, AdminProject> {
        return siblings()
    }
}
