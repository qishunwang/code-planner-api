import FluentPostgreSQL
import Vapor
 
final class ProjectModule: Codable {
     
    var id: Int?
    
    var project_id: Project.ID
    var module_id: Module.ID
    var createdAt: Date?
    var updatedAt: Date?
    var deletedAt: Date?
     
    init(id: Int? = nil
        ,project_id: Project.ID
        ,module_id: Module.ID
        ) {
        self.id = id
        self.project_id = project_id
        self.module_id = module_id
    }
    
    init(_ left: Project, _ right: Module) throws {
        self.module_id = try right.requireID()
        self.project_id = try left.requireID()
    }
}
 
extension ProjectModule: PostgreSQLPivot {
    static var entity = "project_module"
    typealias Left = Project
    typealias Right = Module
    static var leftIDKey: LeftIDKey = \.project_id
    static var rightIDKey: RightIDKey = \.module_id
    static var createdAtKey: TimestampKey? = \.createdAt
    static var deletedAtKey: TimestampKey? = \.deletedAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}
 
extension ProjectModule: Migration {
    typealias Database = PostgreSQLDatabase
}
 
extension ProjectModule: Content {}
 
extension ProjectModule: Parameter {}

extension ProjectModule: ModifiablePivot {}

extension Module {
    var projects: Siblings<Module, Project, ProjectModule> {
        return siblings()
    }
}

extension Project {
    var modules: Siblings<Project, Module, ProjectModule> {
        return siblings()
    }
}
