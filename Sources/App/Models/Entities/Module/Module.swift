import FluentPostgreSQL
import Vapor
 
final class Module: Codable {
    var id: Int?
    
    var name: String
    
    var createdAt: Date?
    var updatedAt: Date?
    var deletedAt: Date?
    
    /// Creates a new `Module`.
    init(id: Int? = nil
        ,name: String
        ) {
        self.id = id
        self.name = name
    }
}


extension Module: PostgreSQLModel {
    static var entity = "modules"
    
    static var createdAtKey: TimestampKey? = \.createdAt
    static var deletedAtKey: TimestampKey? = \.deletedAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}


extension Module: Migration {
    typealias Database = PostgreSQLDatabase
}


extension Module: Content {}


extension Module: Parameter {}

extension Module: Validatable {
    static func validations() throws -> Validations<Module> {
        var validations = Validations(Module.self)
        try validations.add(\.name, !.empty)
        return validations
    }
}
  
