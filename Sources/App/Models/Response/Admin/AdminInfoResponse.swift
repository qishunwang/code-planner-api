import Vapor

struct AdminInfoResponse: Content {
    let username: String
}
