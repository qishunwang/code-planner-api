import Vapor

struct AdminListResponse: Content {
    let admins: [Admin.Public]
    let lower: Int
    let upper: Int
    let total: Int
}
