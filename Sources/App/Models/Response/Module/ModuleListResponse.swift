import Vapor

struct ModuleListResponse: Content {
    let modules: [Module]
    let lower: Int
    let upper: Int
    let total: Int
}
