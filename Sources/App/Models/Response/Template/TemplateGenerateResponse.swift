import Vapor

struct TemplateGenerateResponse: Content {
    let token: String
}
