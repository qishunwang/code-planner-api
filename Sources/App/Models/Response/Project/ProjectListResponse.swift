import Vapor

struct ProjectListResponse: Content {
    let projects: [Project]
    let lower: Int
    let upper: Int
    let total: Int
}
