import Vapor

struct ProjectCreateRequest: Content {
    var name: String
    var packageName: String
}
