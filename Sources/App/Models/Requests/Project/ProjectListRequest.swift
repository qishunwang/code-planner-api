import Vapor

struct ProjectListRequest: Content {
    let lower: Int?
    let upper: Int?
}
