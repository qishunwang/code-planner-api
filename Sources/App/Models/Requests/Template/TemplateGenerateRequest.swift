import Vapor

struct TemplateGenerateRequest: Content {
    var ids: String
    var package: String
}
