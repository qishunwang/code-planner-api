import Vapor

struct ModuleCreateRequest: Content {
    var name: String
}
