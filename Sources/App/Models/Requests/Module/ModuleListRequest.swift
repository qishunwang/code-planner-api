import Vapor

struct ModuleListRequest: Content {
    let lower: Int?
    let upper: Int?
}
