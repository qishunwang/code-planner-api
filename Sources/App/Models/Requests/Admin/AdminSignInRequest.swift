import Vapor

struct AdminSignInRequest: Content {
    var username: String
    var password: String
}
