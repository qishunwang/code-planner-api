import Vapor

struct AdminRegistryRequest: Content {
    var username: String
    var password: String
}
