import Vapor

struct AdminVerifyRequest: Content {
    let token: String
}
