import Vapor

struct AdminResetRequest: Content {
    var token: String
    var password: String
}
