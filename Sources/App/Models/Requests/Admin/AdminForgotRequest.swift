import Vapor

struct AdminForgotRequest: Content {
    var username: String
}
