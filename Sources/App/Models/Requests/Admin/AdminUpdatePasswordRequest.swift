import Vapor

struct AdminUpdatePasswordRequest: Content {
    var password: String
}
