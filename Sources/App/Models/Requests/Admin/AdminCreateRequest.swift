import Vapor

struct AdminCreateRequest: Content {
    var username: String
    var password: String
}
