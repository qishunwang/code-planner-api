# Build Up Project

## Mac OS  
    brew upgrade libressl
## Local Docker

     git clone ....
     cd ./code-planner-api
     cp .env.local.example .env

### Run Up Remote With Proxy Network Docker Stack

     docker-compose up -d
### To Rebuild Image     
     docker-compose up --build
